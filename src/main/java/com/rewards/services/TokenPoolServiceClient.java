package com.rewards.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rewards.dto.TokenPool;

@Service
public class TokenPoolServiceClient {

	public StringBuffer getTokenPool() {

		String uri = "https://129.213.148.83/ords/CMP_MGR_DATA/page/dt_token_pool";
		
		
		/*
		 * RestTemplate restTemplate = new RestTemplate(); HttpHeaders headers = new
		 * HttpHeaders();
		 * headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		 * HttpEntity<String> entity = new HttpEntity<>(headers); ResponseEntity<String>
		 * response = restTemplate.exchange(uri,HttpMethod.GET,entity,String.class);
		 * System.out.println("response - "+response);
		 */
		
		/*
		 * RestTemplate resttemplate = new RestTemplate(); TokenPool tokenlist =
		 * resttemplate.getForObject(uri, TokenPool.class);
		 * System.out.println(tokenlist);
		 */



		//ResponseEntity<List<TokenPool>> responseEntity = resttemplate.getForEntity(uri, Object[].class);



		/*
		 * RestTemplate resttemplate = new RestTemplate();
		 * ResponseEntity<List<TokenPool>> responseEntity = resttemplate.exchange(
		 * "https://129.213.148.83/ords/CMP_MGR_DATA/page/dt_token_pool",HttpMethod.GET,
		 * null, new ParameterizedTypeReference<List<TokenPool>>(){});
		 * System.out.println(responseEntity.getStatusCode()); List<TokenPool> tplist =
		 * responseEntity.getBody(); System.out.println(tplist); //
		 * System.out.println("Size of received list - "+ tplist.size());
		 * 
		 */
		StringBuffer html = new StringBuffer();
		  try {
		  
		  String url = "https://das1.drop-tank.com/ords/STAGINFRAPDB1/page/heart_beat/check";
		  
		  URL obj = new URL(url);
		  HttpURLConnection conn = (HttpURLConnection)obj.openConnection(); 
		  conn.setReadTimeout(5000);
		  conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
		  conn.addRequestProperty("User-Agent", "Mozilla");
		  conn.addRequestProperty("Referer", "google.com");
		  
		  System.out.println("Request URL ... " + url);
		  
		  boolean redirect = false;
		  
		  // normally, 3xx is redirect 
		  int status = conn.getResponseCode(); 
		  if (status != HttpURLConnection.HTTP_OK) 
		  { 
			  if (status ==  HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER) redirect = true; 
		  }
		  
		  System.out.println("Response Code ... " + status);
		  
		  if (redirect) {
		  
		  // get redirect url from "location" header field 
			 String newUrl = conn.getHeaderField("Location");
		  
		  // get the cookie if need, for login 
			 String cookies =  conn.getHeaderField("Set-Cookie");
		  
		  // open the new connnection again 
		  conn = (HttpURLConnection) new  URL(newUrl).openConnection();
		  conn.setRequestProperty("Cookie", cookies);
		  conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
		  conn.addRequestProperty("User-Agent", "Mozilla");
		  conn.addRequestProperty("Referer", "google.com");
		  
		  System.out.println("Redirect to URL : " + url);
		  
		  }
		  
		  BufferedReader in = new BufferedReader( new InputStreamReader(conn.getInputStream())); 
		  String inputLine;
		  
		  
		  while ((inputLine = in.readLine()) != null) 
		  {
			  
			  html.append(inputLine); 
		  } 
		  in.close();
		  
		  System.out.println("URL Content... \n" + html.toString());
		  System.out.println("Done");
		  
		  
		  
		  } catch (Exception e) {
		  
			  e.printStackTrace();
		  }
		return html;
		 
	}
}


