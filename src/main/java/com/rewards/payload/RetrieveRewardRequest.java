package com.rewards.payload;

public class RetrieveRewardRequest {
	
	private Long rewardID;
	private String redemptionLogNum;
	
	
	public Long getRewardID() {
		return rewardID;
	}
	public void setRewardID(Long rewardID) {
		this.rewardID = rewardID;
	}
	public String getRedemptionLogNum() {
		return redemptionLogNum;
	}
	public void setRedemptionLogNum(String redemptionLogNum) {
		this.redemptionLogNum = redemptionLogNum;
	}
	
	

}
