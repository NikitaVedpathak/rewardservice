package com.rewards.payload;

import com.rewards.dto.SiteRedemptionInformation;


public class CurrentRewardRequest {
	
	private String tokenID;
	private SiteRedemptionInformation siteredemptioninformation;
	
	/**
	 * Default constructor
	 */
	public CurrentRewardRequest() {}
	
	/**
	 * parameterized constructor
	 * @param tokenID
	 * @param siteredemptioninformation
	 */
	public CurrentRewardRequest(String tokenID, SiteRedemptionInformation siteredemptioninformation) {
		super();
		this.tokenID = tokenID;
		this.siteredemptioninformation = siteredemptioninformation;
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */

	public String getTokenID() {
		return tokenID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public SiteRedemptionInformation getSiteredemptioninformation() {
		return siteredemptioninformation;
	}

	public void setSiteredemptioninformation(SiteRedemptionInformation siteredemptioninformation) {
		this.siteredemptioninformation = siteredemptioninformation;
	}
	
	

}
