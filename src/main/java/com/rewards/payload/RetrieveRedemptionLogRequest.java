package com.rewards.payload;

import com.rewards.dto.GetRewardsRequest;
import com.rewards.dto.RewardPool;
import com.rewards.dto.SiteRedemptionInformation;

/**
 * Payload for Retrieve redemptionlog request
 * @author Nikita
 *
 */
public class RetrieveRedemptionLogRequest {
	
	private GetRewardsRequest request;
	private SiteRedemptionInformation sri;
	private RewardPool reward;
	
	/**
	 * Default Constructor
	 */
	public RetrieveRedemptionLogRequest() {}

	
	/**
	 * Parameterized constructor
	 * @param request
	 * @param sri
	 * @param reward
	 */
	public RetrieveRedemptionLogRequest(GetRewardsRequest request, SiteRedemptionInformation sri, RewardPool reward) {
		this.request = request;
		this.sri = sri;
		this.reward = reward;
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */


	public GetRewardsRequest getRequest() {
		return request;
	}


	public void setRequest(GetRewardsRequest request) {
		this.request = request;
	}


	public SiteRedemptionInformation getSri() {
		return sri;
	}


	public void setSri(SiteRedemptionInformation sri) {
		this.sri = sri;
	}


	public RewardPool getReward() {
		return reward;
	}


	public void setReward(RewardPool reward) {
		this.reward = reward;
	}
	
	
	
	

}
