package com.rewards.payload;

import java.util.Date;

public class ExceptionResponse {

	private Date timestamp;
	private String SiteStatus;
	/*
	 * private String message; private String details; private String statuscode;
	 */
	
	/**
	 * Default Constructor
	 */
	public ExceptionResponse() {}
	
	/**
	 * 
	 * @param timestamp
	 * @param message
	 * @param details
	 * @param statuscode
	 * 
	 * 
	 */
	public ExceptionResponse(Date timestamp, String message, String details, String statuscode) {
		this.timestamp = timestamp;
		/*
		 * this.message = message; this.details = details; this.statuscode = statuscode;
		 */
	}
	
	
	public ExceptionResponse(Date timestamp, String status)
	{
		this.timestamp = timestamp;
		SiteStatus = status;
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	public String getSiteStatus() {
		return SiteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		SiteStatus = siteStatus;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/*
	 * public String getMessage() { return message; }
	 * 
	 * public void setMessage(String message) { this.message = message; }
	 * 
	 * public String getDetails() { return details; }
	 * 
	 * public void setDetails(String details) { this.details = details; }
	 * 
	 * public String getstatuscode() { return statuscode; }
	 * 
	 * public void setstatuscode(String statuscode) { this.statuscode = statuscode;
	 * }
	 */
	
	

}
