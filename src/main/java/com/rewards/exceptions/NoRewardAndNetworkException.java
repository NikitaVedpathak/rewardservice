package com.rewards.exceptions;

public class NoRewardAndNetworkException extends Exception{
	
	 /**
     * Creates a new instance of <code>NoRewardAndNetworkException</code>
     * without detail message.
     */
    public NoRewardAndNetworkException() {
    }

    /**
     * Constructs an NoRewardAndNetworkException with an error message.
     *
     * @param msg the error message to set
     */
    public NoRewardAndNetworkException(String msg) {
        super(msg);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return super.getCause(); //To change body of generated methods, choose Tools | Templates.
    }

}
