package com.rewards.exceptions;

import com.rewards.dto.LoyaltyErrorCodes;
import com.rewards.dto.RewardPool;
import com.rewards.dto.RewardStatusCodes;

public class RewardLockedStatusException extends Exception {

    private RewardPool reward;
    private LoyaltyErrorCodes errorCode;
    
    /**
     * Constructs a RewardInvalidStatusException with an error message.
     *
     * @param msg the error message to set
     */
    public RewardLockedStatusException(String msg) {
        super(msg);
    }

    public RewardLockedStatusException(LoyaltyErrorCodes errorCode, String msg) {
        this(msg);
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

	/*
	 * public RewardStatusCodes getStatusCode() { return
	 * this.reward.getDtRpStatusCode(); }
	 */
    public RewardPool getReward() {
        return reward;
    }

    @Override
    public synchronized Throwable getCause() {
        return super.getCause(); //To change body of generated methods, choose Tools | Templates.
    }

    public LoyaltyErrorCodes getLoyaltyErrorCode() {
        return this.errorCode;
    }
    
}
