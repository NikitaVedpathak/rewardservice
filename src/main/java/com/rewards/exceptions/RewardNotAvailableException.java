package com.rewards.exceptions;

import com.rewards.dto.LoyaltyErrorCodes;
import com.rewards.dto.TokenPool;

/**
 * Thrown when there is no reward found in the database.
 * @author Nikita
 *
 */
public class RewardNotAvailableException extends Exception {
	
	private String tokenID;
    private String redemptionNum;
    private TokenPool token;
    private LoyaltyErrorCodes errorCode;
    
    /**
     * Constructs a RewardNotFoundException with an error message.
     *
     * @param msg the error message to set
     */
    public RewardNotAvailableException(String msg) {
        super(msg);
    }

    
    /**
     * Constructs a RewardNotFoundException with an error message and a tokenID.
     *
     * @param msg the error message to set
     * @param tokenID the tokenID to set
     */
    public RewardNotAvailableException(String msg, String tokenID) {
        this(msg);
        this.tokenID = this.maskTokenID(tokenID);
    }
    
    public RewardNotAvailableException(LoyaltyErrorCodes errorCode, String msg) {
        this(msg);
        this.tokenID = this.maskTokenID(tokenID);
    }

    public RewardNotAvailableException(String msg, TokenPool token) {
        this(msg);
        this.token = token;
    }
    
    /**
     * Constructs a RewardNotFoundException with an error message, a tokenID and
     * a redemptionNum.
     *
     * @param msg the error message to set
     * @param tokenID the tokenID to set
     * @param redemptionNum the redemptionNum to set
     */
    public RewardNotAvailableException(String msg, String tokenID, String redemptionNum) {
        this(msg, tokenID);
        this.redemptionNum = redemptionNum;
    }
    
    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return super.getCause(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    private String maskTokenID(String tokenID) {
        if (tokenID == null) {
            return "";
        }

        tokenID = tokenID.trim();
        if (tokenID.isEmpty()) {
            return "";
        }

        if (tokenID.length() <= 10) {
            return tokenID;
        } else {
            StringBuffer sb = new StringBuffer().append(tokenID.substring(0, 6));
            int len = tokenID.length() - 10;
            for (int i = 0; i < len; i++) {
                sb.append("*");
            }
            return sb.append(tokenID.subSequence(tokenID.length() - 4, tokenID.length())).toString();
        }
    }

    
    /**
     * Getters and Setters
     */

	public String getTokenID() {
		return tokenID;
	}


	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}


	public String getRedemptionNum() {
		return redemptionNum;
	}


	public void setRedemptionNum(String redemptionNum) {
		this.redemptionNum = redemptionNum;
	}


	public TokenPool getToken() {
		return token;
	}


	public void setToken(TokenPool token) {
		this.token = token;
	}


	public LoyaltyErrorCodes getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(LoyaltyErrorCodes errorCode) {
		this.errorCode = errorCode;
	}
    


    
}
