package com.rewards.webservices;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rewards.dto.RedemptionLogEntry;
import com.rewards.dto.RewardPool;
import com.rewards.dto.TokenPool;
import com.rewards.payload.CurrentRewardRequest;
import com.rewards.payload.ExceptionResponse;
import com.rewards.payload.RetrieveRewardRequest;
import com.rewards.services.TokenPoolServiceClient;

/**
 * Contains implementation and endpoints for the rewards.
 * @author Nikita
 *
 */
@RestController
@RequestMapping(value="/rewards")
public class RewardsRestService {

	Logger logger = Logger.getLogger(RewardsRestService.class);
	
	@Autowired
	private TokenPoolServiceClient tpsc;

	/**
	 * Default constructor
	 */
	public RewardsRestService() {}

	
	/**
	 * This method checks the POSID in site info is present in the DB (for now assume "DT10002" is not present in DB) any other POSID returns back the 
	 * RewardPool Entity in JSon.
	 * Timer is provided  
	 * @param CurrentRewardRequest
	 * @return RewardPool Entity in json
	 */

	@PostMapping(value="/getCurrentReward", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getCurrentRewards(@RequestBody CurrentRewardRequest request )
	{
		logger.info("Received request Body : TokenID"+ request.getTokenID() + "SiteRedemptionInfo"+ request.getSiteredemptioninformation());

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(request.getSiteredemptioninformation().getPosID().equals("DT10002") || !request.getSiteredemptioninformation().getPosID().matches("DT[0-9]*"))
		{
			return new ResponseEntity<>(new RewardPool(), HttpStatus.OK);
		}
		
		String dtRpRlRedemptionNum = "DT_20180531_170530365304676382";
		return new ResponseEntity<RewardPool>(new RewardPool(dtRpRlRedemptionNum),HttpStatus.OK);				
		
	}

	/**
	 * 
	 * @param RewardPool
	 * @return Status String
	 */

	@PostMapping(value="/updateReward") 
	public String updateReward(RewardPool reward) 
	{  
		try
		{
			Thread.sleep(200); 
		}catch (InterruptedException e) { 
			//TODO Auto-generated catch block 
			e.printStackTrace(); 
		}
		return "Updated the Rewards"; 
	}
	
	/**
	 * 
	 * @param RetrieveRewardRequest
	 * @return RewardPool Entity
	 */
	@PostMapping(value="/retrieveReward", produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<RewardPool> retrieveReward(@RequestBody RetrieveRewardRequest request)
	{
		logger.info("Received Request Body : reward ID: "+ request.getRewardID()+"  RedemptionLognum: "+ request.getRedemptionLogNum());

		try
		{ 
			Thread.sleep(200);			
		} catch (InterruptedException e){ 
			// TODO	Auto-generated catch block 
			e.printStackTrace();
		}
		
		if(!request.getRedemptionLogNum().matches("DT_.*"))
		{
			return new ResponseEntity<RewardPool>(new RewardPool(),HttpStatus.OK);
		}	
		
		String dtRpRlRedemptionNum = "DT_20180531_170530365304676382";
		return new ResponseEntity<RewardPool>(new RewardPool(dtRpRlRedemptionNum),HttpStatus.OK);		
	}

	/**	 
	 * 
	 * @param tokenID
	 * @return TokenPool Entity in JSON
	 */
	@GetMapping(value="/retrieveToken/{tokenID}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TokenPool> retrieveToken(@PathVariable String tokenID)
	{
		logger.info("Received request body : tokenid - "+ tokenID);
		try
		{ 
			Thread.sleep(200);			
		} catch (InterruptedException e){ 
			// TODO	Auto-generated catch block 
			e.printStackTrace();
		}		
		
		//tpsc.getTokenPool();
		
		return new ResponseEntity<>(new TokenPool(), HttpStatus.OK);		
	}
	




}
