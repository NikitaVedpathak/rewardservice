package com.rewards.webservices;

import java.util.Date;

import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rewards.dto.RedemptionLogEntry;
import com.rewards.payload.ExceptionResponse;
import com.rewards.payload.RetrieveRedemptionLogRequest;

@RestController
@RequestMapping(value = "/redemptionlog")
public class RedemptionLogService {

	Logger logger = Logger.getLogger(RedemptionLogService.class);

	/**
	 * Default constructor
	 */
	public RedemptionLogService() {}

	@PostMapping(value = "/retrieveredemptionlog", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity retrieveRedemptionLogEntry(@RequestBody RetrieveRedemptionLogRequest request) {
		logger.info("Request Body :  Getrewardsrequestmessage - " + request.getRequest() + "  Siteredemptioninfo - "
				+ request.getSri() + "  RewardPool - " + request.getReward());

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (request.getRequest().getRequestHeader().getStoreLocationID().equals("DT10002") || !request.getRequest().getRequestHeader().getStoreLocationID().matches("DT[0-9]*")) 
		{

			return new ResponseEntity<>(new RedemptionLogEntry(), HttpStatus.OK);

		}
		else
		{
			return new ResponseEntity<>(new RedemptionLogEntry(request.getRequest().getRequestHeader().getStoreLocationID(), request.getRequest().getRequestHeader().getLoyaltySequenceID()), HttpStatus.OK);
		}

	}

	@GetMapping(value = "/retrieveredemptionlog/{redemptionNum}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RedemptionLogEntry> retrieveRedemptionLogEntry(@PathVariable String redemptionNum) {
		logger.info("Request body - " + redemptionNum);
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(!redemptionNum.matches("DT_.*"))
		{
			return new ResponseEntity<>(new RedemptionLogEntry(), HttpStatus.OK);
		}		
		return new ResponseEntity<>(new RedemptionLogEntry(redemptionNum), HttpStatus.OK);
	}

}
