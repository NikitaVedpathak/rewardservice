package com.rewards.webservices;



import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rewards.dto.SiteRedemptionInformation;
import com.rewards.dto.SiteStatus;


@RestController
@RequestMapping(value="/site")
public class SiteRestService {
	
	Logger logger = Logger.getLogger(SiteRestService.class);
	
	/**
	 * Default constructor
	 */
	public SiteRestService() {}
	
	
	@GetMapping(value = "/getSiteInfo/{posID}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity retrieveSiteRedemptionInfo(@PathVariable String posID)
	{
		logger.info("Request request body : "+ posID);
		
		try
		{ 
			Thread.sleep(200);			
		} catch (InterruptedException e){ 
			// TODO	Auto-generated catch block 
			e.printStackTrace();
		}		
		
		/*if(posID.equals("DT10002")) //this is not in system PosID does not match
		{
			ExceptionResponse ex = new ExceptionResponse(new Date(),"POS ID not identified","Exception details","425");
			return new ResponseEntity<ExceptionResponse>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
		}*/
		if(posID.equals("DT10002") || !posID.matches("DT[0-9]*")) //this is not in system PosID does not match
		{
			//ExceptionResponse ex = new ExceptionResponse(new Date(),"POS ID not identified","Exception details","425");
			//ExceptionResponse ex = new ExceptionResponse(new Date(), "INACTIVE");
			SiteRedemptionInformation sri = new SiteRedemptionInformation(posID,SiteStatus.INACTIVE);
			return new ResponseEntity<SiteRedemptionInformation>(sri, HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<SiteRedemptionInformation>(new SiteRedemptionInformation(posID), HttpStatus.OK);
		}
		
	}




}
