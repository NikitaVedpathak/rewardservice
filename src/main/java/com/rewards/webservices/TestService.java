package com.rewards.webservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rewards.services.TokenPoolServiceClient;


@RestController
@RequestMapping(value="/test")
public class TestService {
	
	@Autowired
	private TokenPoolServiceClient tpsc;
	
	@GetMapping(value="/getords")
	public ResponseEntity<String> getResponse()
	{
		StringBuffer obj = tpsc.getTokenPool();
		return new ResponseEntity<>(new String(obj), HttpStatus.OK);
	}

}
