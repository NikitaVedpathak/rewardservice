package com.rewards.dto;

public class Promotion {

	protected String status;	  
	protected PromotionID promotionID;
	protected String promotionReason;
	protected Amount12 promotionAmount;
	protected String loyaltyRewardID;

	public String getLoyaltyRewardID() {


		if (loyaltyRewardID == null){
			loyaltyRewardID = "100";
		}

		return loyaltyRewardID;
	}

	public void setLoyaltyRewardID(String loyaltyRewardID) {
		this.loyaltyRewardID = loyaltyRewardID;
	}

	public boolean isPromotionActive(){

		if(status.equals("normal")){
			return true;
		}

		return false;
	}
	/**
	 * Gets the value of the promotionID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link PromotionID }
	 *     
	 */
	public PromotionID getPromotionID() {
		return promotionID;
	}

	/**
	 * Sets the value of the promotionID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link PromotionID }
	 *     
	 */
	public void setPromotionID(PromotionID value) {
		this.promotionID = value;
	}

	/**
	 * Gets the value of the promotionReason property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPromotionReason() {
		return promotionReason;
	}

	/**
	 * Sets the value of the promotionReason property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPromotionReason(String value) {
		this.promotionReason = value;
	}

	/**
	 * Gets the value of the promotionAmount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getPromotionAmount() {
		return promotionAmount;
	}

	/**
	 * Sets the value of the promotionAmount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setPromotionAmount(Amount12 value) {
		this.promotionAmount = value;
	}

}
