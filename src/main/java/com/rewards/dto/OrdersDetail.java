package com.rewards.dto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a row in the DT_ORDERS_DETAIL database table with each column
 * mapped to a property of this class.
 * @author Nikita
 *
 */


public class OrdersDetail {

	private Integer dtOdId;
	private Date dtOdActivationDate;
	private int dtOdRewardQuantity;
	private BigDecimal dtOdBillableAmount;
	private Orders dtOdOrderSysNum;
	private int dtOdProgId; // changes type from Programs to int
	private int dtOdTpptcProcessingTypeCode; // changed type from TokenPoolProcessingTypeCodesEntity to int
	private List<String> rewardPoolList; // changed to String from RewardPool generics

	/**
	 * Default Constructor
	 */
	public OrdersDetail(){
		
		SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.dtOdId = 0000000001;
		try {
			this.dtOdActivationDate = dateformatter.parse("2015-09-01 00:00:00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dtOdRewardQuantity = 0;
		this.dtOdBillableAmount = new BigDecimal(0.00);
		this.dtOdOrderSysNum = new Orders();
		this.dtOdProgId = 9;
		this.dtOdTpptcProcessingTypeCode = 111;		
		List<String> rlist = new ArrayList<>();
		rlist.add("dummy reward");
		this.rewardPoolList = rlist;
	}


	/**
	 * Parameterized constructor
	 * @param dtOdId
	 * @param dtOdActivationDate
	 * @param dtOdRewardQuantity
	 * @param dtOdBillableAmount
	 * @param dtOdOrderSysNum
	 * @param dtOdProgId
	 * @param dtOdTpptcProcessingTypeCode
	 * @param rewardPoolList
	 */

	public OrdersDetail(Integer dtOdId, Date dtOdActivationDate, int dtOdRewardQuantity, BigDecimal dtOdBillableAmount,
			Orders dtOdOrderSysNum, int dtOdProgId, int dtOdTpptcProcessingTypeCode,
			List<RewardPool> rewardPoolList) {
		this.dtOdId = dtOdId;
		this.dtOdActivationDate = dtOdActivationDate;
		this.dtOdRewardQuantity = dtOdRewardQuantity;
		this.dtOdBillableAmount = dtOdBillableAmount;
		this.dtOdOrderSysNum = dtOdOrderSysNum;
		this.dtOdProgId = dtOdProgId;
		this.dtOdTpptcProcessingTypeCode = dtOdTpptcProcessingTypeCode;
		//this.rewardPoolList = rewardPoolList;
	}


	/**
	 * Getters and Setters
	 */
	public Integer getDtOdId() {
		return dtOdId;
	}


	public void setDtOdId(Integer dtOdId) {
		this.dtOdId = dtOdId;
	}


	public Date getDtOdActivationDate() {
		return dtOdActivationDate;
	}


	public void setDtOdActivationDate(Date dtOdActivationDate) {
		this.dtOdActivationDate = dtOdActivationDate;
	}


	public int getDtOdRewardQuantity() {
		return dtOdRewardQuantity;
	}


	public void setDtOdRewardQuantity(int dtOdRewardQuantity) {
		this.dtOdRewardQuantity = dtOdRewardQuantity;
	}


	public BigDecimal getDtOdBillableAmount() {
		return dtOdBillableAmount;
	}


	public void setDtOdBillableAmount(BigDecimal dtOdBillableAmount) {
		this.dtOdBillableAmount = dtOdBillableAmount;
	}


	public Orders getDtOdOrderSysNum() {
		return dtOdOrderSysNum;
	}


	public void setDtOdOrderSysNum(Orders dtOdOrderSysNum) {
		this.dtOdOrderSysNum = dtOdOrderSysNum;
	}


	public int getDtOdProgId() {
		return dtOdProgId;
	}


	public void setDtOdProgId(int dtOdProgId) {
		this.dtOdProgId = dtOdProgId;
	}


	public int getDtOdTpptcProcessingTypeCode() {
		return dtOdTpptcProcessingTypeCode;
	}


	public void setDtOdTpptcProcessingTypeCode(int dtOdTpptcProcessingTypeCode) {
		this.dtOdTpptcProcessingTypeCode = dtOdTpptcProcessingTypeCode;
	}


	public List<String> getRewardPoolList() {
		return rewardPoolList;
	}


	/*
	 * public void setRewardPoolList(List<RewardPool> rewardPoolList) {
	 * this.rewardPoolList = rewardPoolList; }
	 */

	/**
	 * Hash Code and equals method for the class
	 */
	

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dtOdId != null ? dtOdId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersDetail)) {
            return false;
        }
        OrdersDetail other = (OrdersDetail) object;
        if ((this.dtOdId == null && other.dtOdId != null) || (this.dtOdId != null && !this.dtOdId.equals(other.dtOdId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.droptank.rewards.model.entity.OrdersDetail[ dtOdId=" + dtOdId + " ]";
    }
	
	


}
