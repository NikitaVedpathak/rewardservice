package com.rewards.dto;

public class ItemTax {

	protected String taxLevelID;	 
	protected Amount12 taxRefundedAmount;
	protected Amount12 taxCollectedAmount;

	/**
	 * Gets the value of the taxLevelID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getTaxLevelID() {
		return taxLevelID;
	}

	/**
	 * Sets the value of the taxLevelID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setTaxLevelID(String value) {
		this.taxLevelID = value;
	}

	/**
	 * Gets the value of the taxRefundedAmount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getTaxRefundedAmount() {
		return taxRefundedAmount;
	}

	/**
	 * Sets the value of the taxRefundedAmount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setTaxRefundedAmount(Amount12 value) {
		this.taxRefundedAmount = value;
	}

	/**
	 * Gets the value of the taxCollectedAmount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getTaxCollectedAmount() {
		return taxCollectedAmount;
	}

	/**
	 * Sets the value of the taxCollectedAmount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setTaxCollectedAmount(Amount12 value) {
		this.taxCollectedAmount = value;
	}

}
