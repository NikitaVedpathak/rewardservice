package com.rewards.dto;

import java.util.List;

/**
 *  Represents a row in the DT_REDEMPTION_STATE database table with each column
 * mapped to a property of this class.	
 *
 */
public class RedemptionStateEntity {
	
	private Integer dtRedemptionState;    
    private String dtRedemptionStateDescription;
    private List<String> redemptionLogList;
    
    /**
     * Default Constructor
     */
    public RedemptionStateEntity() {}

    
    /**
     * Parameterized constructor
     * @param dtRedemptionState
     * @param dtRedemptionStateDescription
     * @param redemptionLogList
     */
	public RedemptionStateEntity(Integer dtRedemptionState, String dtRedemptionStateDescription,
			List<String> redemptionLogList) {
		this.dtRedemptionState = dtRedemptionState;
		this.dtRedemptionStateDescription = dtRedemptionStateDescription;
		this.redemptionLogList = redemptionLogList;
	}

	
	/**
	 * Getters and Setters
	 * 
	 */

	public Integer getDtRedemptionState() {
		return dtRedemptionState;
	}


	public void setDtRedemptionState(Integer dtRedemptionState) {
		this.dtRedemptionState = dtRedemptionState;
	}


	public String getDtRedemptionStateDescription() {
		return dtRedemptionStateDescription;
	}


	public void setDtRedemptionStateDescription(String dtRedemptionStateDescription) {
		this.dtRedemptionStateDescription = dtRedemptionStateDescription;
	}


	public List<String> getRedemptionLogList() {
		return redemptionLogList;
	}


	public void setRedemptionLogList(List<String> redemptionLogList) {
		this.redemptionLogList = redemptionLogList;
	}
    
	/**
	 * Hash Code , equals and toString method
	 */

	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (dtRedemptionState != null ? dtRedemptionState.hashCode() : 0);
	        return hash;
	    }

	    @Override
	    public boolean equals(Object object) {
	        // TODO: Warning - this method won't work in the case the id fields are not set
	        if (!(object instanceof RedemptionStateEntity)) {
	            return false;
	        }
	        RedemptionStateEntity other = (RedemptionStateEntity) object;
	        if ((this.dtRedemptionState == null && other.dtRedemptionState != null) || (this.dtRedemptionState != null && !this.dtRedemptionState.equals(other.dtRedemptionState))) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        return "com.droptank.rlms.model.entities.RedemptionState[ dtRedemptionState=" + dtRedemptionState + " ]";
	    }
    
    

}
