package com.rewards.dto;

import java.util.ArrayList;
import java.util.List;

public class TransactionLine {
	

 
    protected int lineNumber;
    protected List<TenderInfo> tenderInfos;
    protected TransactionTax transactionTax;
    protected FuelLine fuelLine;
    protected MerchandiseCodeLine merchandiseCodeLine;
    protected ItemLine itemLine;
    protected List<Integer> extensions;
    protected TransactionLineStatus status;

    /**
     * Gets the value of the lineNumber property.
     * 
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     */
    public void setLineNumber(int value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the tenderInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderInfo }
     * 
     * 
     */
    public List<TenderInfo> getTenderInfos() {
        if (tenderInfos == null) {
            tenderInfos = new ArrayList<TenderInfo>();
        }
        return this.tenderInfos;
    }

    /**
     * Gets the value of the transactionTax property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionTax }
     *     
     */
    public TransactionTax getTransactionTax() {
        return transactionTax;
    }

    /**
     * Sets the value of the transactionTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionTax }
     *     
     */
    public void setTransactionTax(TransactionTax value) {
        this.transactionTax = value;
    }

    /**
     * Gets the value of the fuelLine property.
     * 
     * @return
     *     possible object is
     *     {@link FuelLine }
     *     
     */
    public FuelLine getFuelLine() {
        return fuelLine;
    }

    /**
     * Sets the value of the fuelLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link FuelLine }
     *     
     */
    public void setFuelLine(FuelLine value) {
        this.fuelLine = value;
    }

    /**
     * Gets the value of the merchandiseCodeLine property.
     * 
     * @return
     *     possible object is
     *     {@link MerchandiseCodeLine }
     *     
     */
    public MerchandiseCodeLine getMerchandiseCodeLine() {
        return merchandiseCodeLine;
    }

    /**
     * Sets the value of the merchandiseCodeLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchandiseCodeLine }
     *     
     */
    public void setMerchandiseCodeLine(MerchandiseCodeLine value) {
        this.merchandiseCodeLine = value;
    }

    /**
     * Gets the value of the itemLine property.
     * 
     * @return
     *     possible object is
     *     {@link ItemLine }
     *     
     */
    public ItemLine getItemLine() {
        return itemLine;
    }

    /**
     * Sets the value of the itemLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemLine }
     *     
     */
    public void setItemLine(ItemLine value) {
        this.itemLine = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extensions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtensions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link com.droptank.loyalty.pli.BusinessPeriod.Extension }
     * 
     * 
     */
    public List<Integer> getExtensions() {
        if (extensions == null) {
            extensions = new ArrayList<Integer>();
        }
        return this.extensions;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionLineStatus }
     *     
     */
    public TransactionLineStatus getStatus() {
        if (status == null) {
            return TransactionLineStatus.NORMAL;
        } else {
            return status;
        }
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionLineStatus }
     *     
     */
    public void setStatus(TransactionLineStatus value) {
        this.status = value;
    }

    public String getLineNumberAsString(){
        
        return Integer.toString(lineNumber);
        
    }

}
