package com.rewards.dto;

import org.springframework.stereotype.Component;


public enum SiteStatus {

	INACTIVE, ACTIVE, CLOSED;
	
}
