package com.rewards.dto;

import java.math.BigDecimal;

public class Quantity12 {

	protected BigDecimal value;
	protected String uom;

	/**
	 *  12,5 decimal value
	 * 
	 * @return
	 *     possible object is
	 *     {@link BigDecimal }
	 *     
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Sets the value of the value property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link BigDecimal }
	 *     
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * Gets the value of the uom property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getUom() {
		if (uom == null) {
			return "each";
		} else {
			return uom;
		}
	}

	/**
	 * Sets the value of the uom property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setUom(String value) {
		this.uom = value;
	}

}
