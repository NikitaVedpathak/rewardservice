package com.rewards.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * POJO for site redemption information
 * @author Nikita
 *
 */


public class SiteRedemptionInformation {

	private String posID;
	private SiteStatus siteStatus; 
	private RedemptionStatus siteRedemptionStatus; 
	private List<Integer> networkIDs;
	private BigDecimal transactionFee;

	/**
	 * Default Constructor
	 */
	public SiteRedemptionInformation() {}	
	
	public SiteRedemptionInformation(String POSID) {
		
		this.posID = POSID;
		this.siteStatus = SiteStatus.ACTIVE;
		this.siteRedemptionStatus = RedemptionStatus.ACTIVE;
		List<Integer> list = new ArrayList<Integer>();
		list.add(1112);
		this.networkIDs = list;		
		this.transactionFee = new BigDecimal(0.00);
		
		
	}

	/**
	 * 
	 * Parameterized constructor
	 */

	public SiteRedemptionInformation(String posID, SiteStatus siteStatus) {
		this.posID = posID;
		this.siteStatus = siteStatus;
		this.siteRedemptionStatus = RedemptionStatus.INACTIVE;
		List<Integer> list = new ArrayList<Integer>();
		this.networkIDs = list;
		this.transactionFee = new BigDecimal(0.00);
	}


	/**
	 * Getter and Setters
	 * 
	 */

	public String getPosID() {
		return posID;
	}
	public void setPosID(String posID) {
		this.posID = posID;
	}
	public SiteStatus getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(SiteStatus siteStatus) {
		this.siteStatus = siteStatus;
	}
	public RedemptionStatus getSiteRedemptionStatus() {
		return siteRedemptionStatus;
	}
	public void setSiteRedemptionStatus(RedemptionStatus siteRedemptionStatus) {
		this.siteRedemptionStatus = siteRedemptionStatus;
	}
	public List<Integer> getNetworkIDs() {
		return networkIDs;
	}
	public void setNetworkIDs(List<Integer> networkIDs) {
		this.networkIDs = networkIDs;
	}
	public BigDecimal getTransactionFee() {
		return transactionFee;
	}
	public void setTransactionFee(BigDecimal transactionFee) {
		this.transactionFee = transactionFee;
	}

	/**
	 * This method checks if the site status is active and site redemption status is active if yes then returns true else false
	 * @return boolean value 
	 */

	public boolean isSiteActive() {
		return ((SiteStatus.ACTIVE == siteStatus) && (RedemptionStatus.ACTIVE == siteRedemptionStatus));
	}

	
    /**
     * This method checks if the site is active or not and then checks if the network id  is active for the site or not 
     * 
     * @param networkID Network ID 
     * @return boolean
     */
    public boolean isSiteActive(Integer networkID) {
        return (this.isSiteActive() && this.isParticipateInNetwork(networkID));
    }


    /**
     * This method checks if the network ID participates 
     * @param networkID
     * @return boolean
     */
    public boolean isParticipateInNetwork(int networkID) {
        return networkIDs.contains(networkID);
    }


    


}
