package com.rewards.dto;

public class POSCodeFormat {

	protected String format;
	protected String checkDigit;

	/**
	 * Gets the value of the format property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the value of the format property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setFormat(String value) {
		this.format = value;
	}

	/**
	 * Gets the value of the checkDigit property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getCheckDigit() {
		return checkDigit;
	}

	/**
	 * Sets the value of the checkDigit property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setCheckDigit(String value) {
		this.checkDigit = value;
	}

}
