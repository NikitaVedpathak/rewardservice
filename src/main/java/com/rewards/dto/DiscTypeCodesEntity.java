package com.rewards.dto;

import java.util.List;

/**
 * Represents a row in the DT_DISC_TYPE_CODES database table with each column mapped to a
 * property of this class
 *
 */
public class DiscTypeCodesEntity {

	private Integer dtDiscTypeCode;
	private String dtDiscTypeCodeDescription;
	private List<String> redemptionLogList;

	/**
	 * Default constructor
	 */
	public DiscTypeCodesEntity() {}

	/**
	 * Parameterized constructor
	 * @param dtDiscTypeCode
	 * @param dtDiscTypeCodeDescription
	 * @param redemptionLogList
	 */
	public DiscTypeCodesEntity(Integer dtDiscTypeCode, String dtDiscTypeCodeDescription,
			List<String> redemptionLogList) {
		this.dtDiscTypeCode = dtDiscTypeCode;
		this.dtDiscTypeCodeDescription = dtDiscTypeCodeDescription;
		this.redemptionLogList = redemptionLogList;
	}

	/**
	 * Getters and Setters
	 * @return
	 */

	public Integer getDtDiscTypeCode() {
		return dtDiscTypeCode;
	}

	public void setDtDiscTypeCode(Integer dtDiscTypeCode) {
		this.dtDiscTypeCode = dtDiscTypeCode;
	}

	public String getDtDiscTypeCodeDescription() {
		return dtDiscTypeCodeDescription;
	}

	public void setDtDiscTypeCodeDescription(String dtDiscTypeCodeDescription) {
		this.dtDiscTypeCodeDescription = dtDiscTypeCodeDescription;
	}

	public List<String> getRedemptionLogList() {
		return redemptionLogList;
	}

	public void setRedemptionLogList(List<String> redemptionLogList) {
		this.redemptionLogList = redemptionLogList;
	}

	
	/**
	 * Hash Code, equals and toString methods
	 */
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtDiscTypeCode != null ? dtDiscTypeCode.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof DiscTypeCodesEntity)) {
			return false;
		}
		DiscTypeCodesEntity other = (DiscTypeCodesEntity) object;
		if ((this.dtDiscTypeCode == null && other.dtDiscTypeCode != null) || (this.dtDiscTypeCode != null && !this.dtDiscTypeCode.equals(other.dtDiscTypeCode))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rlms.model.entities.DiscTypeCodes[ dtDiscTypeCode=" + dtDiscTypeCode + " ]";
	}


}
