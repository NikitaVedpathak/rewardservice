package com.rewards.dto;

import java.math.BigInteger;

public class LinkedTransactionInfo {

	protected String originalStoreLocationID;
	protected BigInteger originalKioskID;
	protected Integer originalFuelPositionID;
	protected BigInteger originalRegisterID;
	protected String originalPOSTransactionID;
	protected String originalLoyaltySequenceID;

	/**
	 * Gets the value of the originalStoreLocationID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getOriginalStoreLocationID() {
		return originalStoreLocationID;
	}

	/**
	 * Sets the value of the originalStoreLocationID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setOriginalStoreLocationID(String value) {
		this.originalStoreLocationID = value;
	}

	/**
	 * Gets the value of the originalKioskID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link BigInteger }
	 *     
	 */
	public BigInteger getOriginalKioskID() {
		return originalKioskID;
	}

	/**
	 * Sets the value of the originalKioskID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link BigInteger }
	 *     
	 */
	public void setOriginalKioskID(BigInteger value) {
		this.originalKioskID = value;
	}

	/**
	 * Gets the value of the originalFuelPositionID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Integer }
	 *     
	 */
	public Integer getOriginalFuelPositionID() {
		return originalFuelPositionID;
	}

	/**
	 * Sets the value of the originalFuelPositionID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Integer }
	 *     
	 */
	public void setOriginalFuelPositionID(Integer value) {
		this.originalFuelPositionID = value;
	}

	/**
	 * Gets the value of the originalRegisterID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link BigInteger }
	 *     
	 */
	public BigInteger getOriginalRegisterID() {
		return originalRegisterID;
	}

	/**
	 * Sets the value of the originalRegisterID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link BigInteger }
	 *     
	 */
	public void setOriginalRegisterID(BigInteger value) {
		this.originalRegisterID = value;
	}

	/**
	 * Gets the value of the originalPOSTransactionID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getOriginalPOSTransactionID() {
		return originalPOSTransactionID;
	}

	/**
	 * Sets the value of the originalPOSTransactionID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setOriginalPOSTransactionID(String value) {
		this.originalPOSTransactionID = value;
	}

	/**
	 * Gets the value of the originalLoyaltySequenceID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getOriginalLoyaltySequenceID() {
		return originalLoyaltySequenceID;
	}

	/**
	 * Sets the value of the originalLoyaltySequenceID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setOriginalLoyaltySequenceID(String value) {
		this.originalLoyaltySequenceID = value;
	}

}
