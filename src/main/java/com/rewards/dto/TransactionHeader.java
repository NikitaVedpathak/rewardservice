package com.rewards.dto;

import java.math.BigInteger;

import javax.xml.datatype.XMLGregorianCalendar;

public class TransactionHeader {
	
	

   
    protected int trainingModeFlag;
    protected int outsideSalesFlag;
    protected BigInteger kioskID;
    protected Integer fuelPositionID;
    protected BigInteger registerID;
    protected String cashierID;
    protected String tillID;
    protected String posTransactionID;
    protected BusinessPeriod businessPeriod;
    protected XMLGregorianCalendar eventStartDate;
    protected XMLGregorianCalendar eventStartTime;
    protected XMLGregorianCalendar eventEndDate;
    protected XMLGregorianCalendar eventEndTime;
    protected LinkedTransactionInfo linkedTransactionInfo;

    
    //private final FlagDefYes YES = new FlagDefYes();
    
    public int isOutSideSale(){
      
//         FlagDefYes YES = new FlagDefYes();
//        return outsideSalesFlag.equals(YES.value);
    	outsideSalesFlag = 1;
         return outsideSalesFlag;
    }
    /**
     * Gets the value of the trainingModeFlag property.
     * 
     * @return
     *     possible object is
     *     {@link FlagDefNo }
     *     
     */
    public int getTrainingModeFlag() {
        return trainingModeFlag;
    }

    /**
     * Sets the value of the trainingModeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlagDefNo }
     *     
     */
    public void setTrainingModeFlag(int value) {
        this.trainingModeFlag = value;
    }

    /**
     * Gets the value of the outsideSalesFlag property.
     * 
     * @return
     *     possible object is
     *     {@link FlagDefYes }
     *     
     */
    public int getOutsideSalesFlag() {
        return outsideSalesFlag;
    }

    /**
     * Sets the value of the outsideSalesFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlagDefYes }
     *     
     */
    public void setOutsideSalesFlag(int value) {
        this.outsideSalesFlag = value;
    }

    /**
     * Gets the value of the kioskID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getKioskID() {
        return kioskID;
    }

    /**
     * Sets the value of the kioskID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setKioskID(BigInteger value) {
        this.kioskID = value;
    }

    /**
     * Gets the value of the fuelPositionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFuelPositionID() {
        return fuelPositionID;
    }

    /**
     * Sets the value of the fuelPositionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFuelPositionID(Integer value) {
        this.fuelPositionID = value;
    }

    /**
     * Gets the value of the registerID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRegisterID() {
        return registerID;
    }

    /**
     * Sets the value of the registerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRegisterID(BigInteger value) {
        this.registerID = value;
    }

    /**
     * Gets the value of the cashierID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashierID() {
        return cashierID;
    }

    /**
     * Sets the value of the cashierID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashierID(String value) {
        this.cashierID = value;
    }

    /**
     * Gets the value of the tillID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTillID() {
        return tillID;
    }

    /**
     * Sets the value of the tillID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTillID(String value) {
        this.tillID = value;
    }

    /**
     * Gets the value of the posTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSTransactionID() {
        return posTransactionID;
    }

    /**
     * Sets the value of the posTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSTransactionID(String value) {
        this.posTransactionID = value;
    }

    /**
     * Gets the value of the businessPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessPeriod }
     *     
     */
    public BusinessPeriod getBusinessPeriod() {
        return businessPeriod;
    }

    /**
     * Sets the value of the businessPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessPeriod }
     *     
     */
    public void setBusinessPeriod(BusinessPeriod value) {
        this.businessPeriod = value;
    }

    /**
     * Gets the value of the eventStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventStartDate() {
        return eventStartDate;
    }

    /**
     * Sets the value of the eventStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventStartDate(XMLGregorianCalendar value) {
        this.eventStartDate = value;
    }

    /**
     * Gets the value of the eventStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventStartTime() {
        return eventStartTime;
    }

    /**
     * Sets the value of the eventStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventStartTime(XMLGregorianCalendar value) {
        this.eventStartTime = value;
    }

    /**
     * Gets the value of the eventEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventEndDate() {
        return eventEndDate;
    }

    /**
     * Sets the value of the eventEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventEndDate(XMLGregorianCalendar value) {
        this.eventEndDate = value;
    }

    /**
     * Gets the value of the eventEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventEndTime() {
        return eventEndTime;
    }

    /**
     * Sets the value of the eventEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventEndTime(XMLGregorianCalendar value) {
        this.eventEndTime = value;
    }

    /**
     * Gets the value of the linkedTransactionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LinkedTransactionInfo }
     *     
     */
    public LinkedTransactionInfo getLinkedTransactionInfo() {
        return linkedTransactionInfo;
    }

    /**
     * Sets the value of the linkedTransactionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkedTransactionInfo }
     *     
     */
    public void setLinkedTransactionInfo(LinkedTransactionInfo value) {
        this.linkedTransactionInfo = value;
    }

}
