package com.rewards.dto;

import java.util.ArrayList;
import java.util.List;

public class TransactionDetailGroup {
	
	 protected List<TransactionLine> transactionLines;
	 
	    public List<TransactionLine> getTransactionLines() {
	        if (transactionLines == null) {
	            transactionLines = new ArrayList<TransactionLine>();
	        }
	        return this.transactionLines;
	    }

}
