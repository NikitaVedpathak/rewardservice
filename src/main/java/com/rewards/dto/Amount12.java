package com.rewards.dto;

public class Amount12 {

	protected String value;
	protected CurrencyCode currency;

	
	/**
	 * Default Constructor
	 */
	public Amount12(){}


	/**
	 * Getters and Setters
	 * @return
	 */

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public CurrencyCode getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyCode currency) {
		this.currency = currency;
	}




}
