package com.rewards.dto;

import java.math.BigInteger;

public class ItemCode {
	
	protected POSCodeFormat posCodeFormat;
    protected BigInteger posCode;
    protected POSCodeModifier posCodeModifier;
    protected String inventoryItemID;

    /**
     * Gets the value of the posCodeFormat property.
     * 
     * @return
     *     possible object is
     *     {@link POSCodeFormat }
     *     
     */
    public POSCodeFormat getPOSCodeFormat() {
        return posCodeFormat;
    }

    /**
     * Sets the value of the posCodeFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link POSCodeFormat }
     *     
     */
    public void setPOSCodeFormat(POSCodeFormat value) {
        this.posCodeFormat = value;
    }

    /**
     * Gets the value of the posCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPOSCode() {
        return posCode;
    }

    /**
     * Sets the value of the posCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPOSCode(BigInteger value) {
        this.posCode = value;
    }

    /**
     * Gets the value of the posCodeModifier property.
     * 
     * @return
     *     possible object is
     *     {@link POSCodeModifier }
     *     
     */
    public POSCodeModifier getPOSCodeModifier() {
        return posCodeModifier;
    }

    /**
     * Sets the value of the posCodeModifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link POSCodeModifier }
     *     
     */
    public void setPOSCodeModifier(POSCodeModifier value) {
        this.posCodeModifier = value;
    }

    /**
     * Gets the value of the inventoryItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInventoryItemID() {
        return inventoryItemID;
    }

    /**
     * Sets the value of the inventoryItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInventoryItemID(String value) {
        this.inventoryItemID = value;
    }

}
