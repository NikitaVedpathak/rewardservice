package com.rewards.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class FuelLine {
	
	
	    protected String fuelGradeID;
	    protected int fuelPositionID;
	    protected BigInteger priceTierCode;
	    protected String timeTierCode;
	    protected String serviceLevelCode;
	    protected String description;
	    protected Amount12 actualSalesPrice;
	    protected Amount12 regularSellPrice;
	    protected Quantity12 salesQuantity;
	    protected Amount12 salesAmount;
	    protected String salesUOM;
	    protected List<Promotion> promotions;
	    protected List<Discount> discounts;
	    protected List<ItemTax> itemTaxes;
	    protected String paymentSystemsProductCode;
	    protected int fuelPrepayFlag;

	    /**
	     * Gets the value of the fuelGradeID property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getFuelGradeID() {
	        return fuelGradeID;
	    }

	    /**
	     * Sets the value of the fuelGradeID property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setFuelGradeID(String value) {
	        this.fuelGradeID = value;
	    }

	    /**
	     * Gets the value of the fuelPositionID property.
	     * 
	     */
	    public int getFuelPositionID() {
	        return fuelPositionID;
	    }

	    /**
	     * Sets the value of the fuelPositionID property.
	     * 
	     */
	    public void setFuelPositionID(int value) {
	        this.fuelPositionID = value;
	    }

	    /**
	     * Gets the value of the priceTierCode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link BigInteger }
	     *     
	     */
	    public BigInteger getPriceTierCode() {
	        return priceTierCode;
	    }

	    /**
	     * Sets the value of the priceTierCode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link BigInteger }
	     *     
	     */
	    public void setPriceTierCode(BigInteger value) {
	        this.priceTierCode = value;
	    }

	    /**
	     * Gets the value of the timeTierCode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getTimeTierCode() {
	        return timeTierCode;
	    }

	    /**
	     * Sets the value of the timeTierCode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setTimeTierCode(String value) {
	        this.timeTierCode = value;
	    }

	    /**
	     * Gets the value of the serviceLevelCode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getServiceLevelCode() {
	        return serviceLevelCode;
	    }

	    /**
	     * Sets the value of the serviceLevelCode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setServiceLevelCode(String value) {
	        this.serviceLevelCode = value;
	    }

	    /**
	     * Gets the value of the description property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getDescription() {
	        return description;
	    }

	    /**
	     * Sets the value of the description property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setDescription(String value) {
	        this.description = value;
	    }

	    /**
	     * Gets the value of the actualSalesPrice property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link Amount12 }
	     *     
	     */
	    public Amount12 getActualSalesPrice() {
	        return actualSalesPrice;
	    }

	    /**
	     * Sets the value of the actualSalesPrice property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link Amount12 }
	     *     
	     */
	    public void setActualSalesPrice(Amount12 value) {
	        this.actualSalesPrice = value;
	    }

	    /**
	     * Gets the value of the regularSellPrice property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link Amount12 }
	     *     
	     */
	    public Amount12 getRegularSellPrice() {
	        return regularSellPrice;
	    }

	    /**
	     * Sets the value of the regularSellPrice property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link Amount12 }
	     *     
	     */
	    public void setRegularSellPrice(Amount12 value) {
	        this.regularSellPrice = value;
	    }

	    /**
	     * Gets the value of the salesQuantity property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link Quantity12 }
	     *     
	     */
	    public Quantity12 getSalesQuantity() {
	        return salesQuantity;
	    }

	    /**
	     * Sets the value of the salesQuantity property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link Quantity12 }
	     *     
	     */
	    public void setSalesQuantity(Quantity12 value) {
	        this.salesQuantity = value;
	    }

	    /**
	     * Gets the value of the salesAmount property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link Amount12 }
	     *     
	     */
	    public Amount12 getSalesAmount() {
	        return salesAmount;
	    }

	    /**
	     * Sets the value of the salesAmount property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link Amount12 }
	     *     
	     */
	    public void setSalesAmount(Amount12 value) {
	        this.salesAmount = value;
	    }

	    /**
	     * Gets the value of the salesUOM property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getSalesUOM() {
	        return salesUOM;
	    }

	    /**
	     * Sets the value of the salesUOM property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setSalesUOM(String value) {
	        this.salesUOM = value;
	    }

	    /**
	     * Gets the value of the promotions property.
	     * 
	     * <p>
	     * This accessor method returns a reference to the live list,
	     * not a snapshot. Therefore any modification you make to the
	     * returned list will be present inside the JAXB object.
	     * This is why there is not a <CODE>set</CODE> method for the promotions property.
	     * 
	     * <p>
	     * For example, to add a new item, do as follows:
	     * <pre>
	     *    getPromotions().add(newItem);
	     * </pre>
	     * 
	     * 
	     * <p>
	     * Objects of the following type(s) are allowed in the list
	     * {@link Promotion }
	     * 
	     * 
	     */
	    public List<Promotion> getPromotions() {
	        if (promotions == null) {
	            promotions = new ArrayList<Promotion>();
	        }
	        return this.promotions;
	    }

	    /**
	     * Gets the value of the discounts property.
	     * 
	     * <p>
	     * This accessor method returns a reference to the live list,
	     * not a snapshot. Therefore any modification you make to the
	     * returned list will be present inside the JAXB object.
	     * This is why there is not a <CODE>set</CODE> method for the discounts property.
	     * 
	     * <p>
	     * For example, to add a new item, do as follows:
	     * <pre>
	     *    getDiscounts().add(newItem);
	     * </pre>
	     * 
	     * 
	     * <p>
	     * Objects of the following type(s) are allowed in the list
	     * {@link Discount }
	     * 
	     * 
	     */
	    public List<Discount> getDiscounts() {
	        if (discounts == null) {
	            discounts = new ArrayList<Discount>();
	        }
	        return this.discounts;
	    }

	    /**
	     * Gets the value of the itemTaxes property.
	     * 
	     * <p>
	     * This accessor method returns a reference to the live list,
	     * not a snapshot. Therefore any modification you make to the
	     * returned list will be present inside the JAXB object.
	     * This is why there is not a <CODE>set</CODE> method for the itemTaxes property.
	     * 
	     * <p>
	     * For example, to add a new item, do as follows:
	     * <pre>
	     *    getItemTaxes().add(newItem);
	     * </pre>
	     * 
	     * 
	     * <p>
	     * Objects of the following type(s) are allowed in the list
	     * {@link ItemTax }
	     * 
	     * 
	     */
	    public List<ItemTax> getItemTaxes() {
	        if (itemTaxes == null) {
	            itemTaxes = new ArrayList<ItemTax>();
	        }
	        return this.itemTaxes;
	    }

	    /**
	     * Gets the value of the paymentSystemsProductCode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getPaymentSystemsProductCode() {
	        return paymentSystemsProductCode;
	    }

	    /**
	     * Sets the value of the paymentSystemsProductCode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setPaymentSystemsProductCode(String value) {
	        this.paymentSystemsProductCode = value;
	    }

	    /**
	     * Gets the value of the fuelPrepayFlag property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link YesNo }
	     *     
	     */
	    public int getFuelPrepayFlag() {
	        return fuelPrepayFlag;
	    }

	    /**
	     * Sets the value of the fuelPrepayFlag property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link YesNo }
	     *     
	     */
	    public void setFuelPrepayFlag(int value) {
	        this.fuelPrepayFlag = value;
	    }
	    
	    public boolean isPrepay(){
	        return true;
	    }
	    
	    public BigDecimal getDiscountTotal(){
	        
	        return new BigDecimal(promotions.get(0).getPromotionAmount().getValue());
	        
	    }

}
