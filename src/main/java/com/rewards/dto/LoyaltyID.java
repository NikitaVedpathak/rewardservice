package com.rewards.dto;


public class LoyaltyID {

	protected String value;
	protected String entryMethod;

	/**
	 * Default constructor
	 */
	public LoyaltyID() {}

	
	/**
	 * Parameterized constructor
	 * @param value
	 * @param entryMethod
	 */
	public LoyaltyID(String value, String entryMethod) {
		this.value = value;
		this.entryMethod = entryMethod;
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getEntryMethod() {
		return entryMethod;
	}


	public void setEntryMethod(String entryMethod) {
		this.entryMethod = entryMethod;
	}
	
	
	
	

}
