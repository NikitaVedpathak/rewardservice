package com.rewards.dto;

import java.util.Date;
import java.util.List;

public class RequestHeader {

	protected String posLoyaltyInterfaceVersion;
	protected String vendorName;
	protected String vendorModelVersion;
	protected String posSequenceID;
	protected String loyaltySequenceID;
	protected String storeLocationID;
	protected int loyaltyOfflineFlag;  //type of class flagDefNo which inturn used enum of YES NO. So changed this to int 0.No, 1.Yes
	protected List<Integer> extensions; // List had generics for com.droptank.loyalty.gilbarco.BusinessPeriod.Extension

	public Date getTransactionTimeStamp(){
		throw new UnsupportedOperationException("[getRequestHeaderAsBytes]-->Unsupported Operation");
	}

	public byte[] getRequestHeaderAsBytes(){
		throw new UnsupportedOperationException("[getRequestHeaderAsBytes]-->Unsupported Operation");
	}

	/**
	 * Default Constructor
	 */
	public RequestHeader() {}
	
	
	/**
	 * Parameterized constructor
	 * @param posLoyaltyInterfaceVersion
	 * @param vendorName
	 * @param vendorModelVersion
	 * @param posSequenceID
	 * @param loyaltySequenceID
	 * @param storeLocationID
	 * @param loyaltyOfflineFlag
	 * @param extensions
	 */

	public RequestHeader(String posLoyaltyInterfaceVersion, String vendorName, String vendorModelVersion,
			String posSequenceID, String loyaltySequenceID, String storeLocationID, int loyaltyOfflineFlag,
			List<Integer> extensions) {
		this.posLoyaltyInterfaceVersion = posLoyaltyInterfaceVersion;
		this.vendorName = vendorName;
		this.vendorModelVersion = vendorModelVersion;
		this.posSequenceID = posSequenceID;
		this.loyaltySequenceID = loyaltySequenceID;
		this.storeLocationID = storeLocationID;
		this.loyaltyOfflineFlag = loyaltyOfflineFlag;
		this.extensions = extensions;
	}
	
	/**
	 * Getters and Setters
	 */

	public String getPosLoyaltyInterfaceVersion() {
		return posLoyaltyInterfaceVersion;
	}

	public void setPosLoyaltyInterfaceVersion(String posLoyaltyInterfaceVersion) {
		this.posLoyaltyInterfaceVersion = posLoyaltyInterfaceVersion;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorModelVersion() {
		return vendorModelVersion;
	}

	public void setVendorModelVersion(String vendorModelVersion) {
		this.vendorModelVersion = vendorModelVersion;
	}

	public String getPosSequenceID() {
		return posSequenceID;
	}

	public void setPosSequenceID(String posSequenceID) {
		this.posSequenceID = posSequenceID;
	}

	public String getLoyaltySequenceID() {
		return loyaltySequenceID;
	}

	public void setLoyaltySequenceID(String loyaltySequenceID) {
		this.loyaltySequenceID = loyaltySequenceID;
	}

	public String getStoreLocationID() {
		return storeLocationID;
	}

	public void setStoreLocationID(String storeLocationID) {
		this.storeLocationID = storeLocationID;
	}

	public int getLoyaltyOfflineFlag() {
		return loyaltyOfflineFlag;
	}

	public void setLoyaltyOfflineFlag(int loyaltyOfflineFlag) {
		this.loyaltyOfflineFlag = loyaltyOfflineFlag;
	}

	public List<Integer> getExtensions() {
		return extensions;
	}

	public void setExtensions(List<Integer> extensions) {
		this.extensions = extensions;
	}
	
	
	
	
	

}
