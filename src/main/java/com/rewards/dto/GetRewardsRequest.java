package com.rewards.dto;

import java.math.BigInteger;

/**
 * 
 *
 */
public class GetRewardsRequest {

	protected RequestHeader requestHeader;
	protected LoyaltyID loyaltyID;
	protected FuelPriceData fuelPriceData;
	protected TransactionData transactionData;


	public int getMessageStatus() {

		boolean fuelData = this.hasFuelData();
		boolean transData = this.hasTransactionData();
		boolean outSideTransaction = this.isOutSideTransaction();
		boolean prepayTransaction = this.isPrepay();
		boolean promotionApplied = this.hasPromotionBeenApplied();
		if (fuelData && transData && !prepayTransaction && !promotionApplied) {
			//            tranState = TransState.LOYALTY_INSIDE_POST_PAY_INSIDE;
			//            System.out.println("[113]:GetRewardsRequest: LOYALTY_INSIDE_POST_PAY_INSIDE");
			return 1;

		} else if (fuelData && transData && !outSideTransaction && prepayTransaction) {
			//            tranState = TransState.LOYALTY_INSIDE_PRE_PAY_INSIDE;
			//            System.out.println("[117]:GetRewardsRequest: LOYALTY_INSIDE_PRE_PAY_INSIDE");
			return 0;

		} else if (fuelData && transData && promotionApplied) {
			//            tranState = TransState.LOYALTY_OUT_SIDE_POST_PAY_INSIDE;
			//            System.out.println("[127]:GetRewardsRequest: LOYALTY_OUT_SIDE_POST_PAY_INSIDE");
			return 1;

		} else if (fuelData) {
			//            tranState = TransState.PRE_PAY;
			//            System.out.println("[132]:GetRewardsRequest: PRE_PAY");
			return 0;
		} else {
			//            tranState = TransState.LOYALTY_BEFORE_TENDER;
			//            System.out.println("[136]:GetRewardsRequest: LOYALTY_BEFORE_TENDER");
			return -1;
		}
	}

	public TransState getMessageType() {
		throw new UnsupportedOperationException();

	}

	public String returnLoyaltyID() {
		return this.getLoyaltyID().getValue();

	}

	public LoyaltyID getLoyaltyID() {

		if (loyaltyID == null) {
			loyaltyID = new LoyaltyID();
		}

		return loyaltyID;
	}

	/**
	 * Gets the value of the requestHeader property.
	 *
	 * @return possible object is {@link RequestHeader }
	 *
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 *
	 * @param value allowed object is {@link RequestHeader }
	 *
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}




	public boolean isPrepay() {
		boolean response = false;

		if (transactionData == null || transactionData.isPrepayTransaction()) {
			response = true;
		}

		return response;
	}


	public int getFuelLineNumberToAddRewardTo() {

		int response = -1;

		if (transactionData != null) {
			response = transactionData.getFuelLineNumberForAddReward();
		}

		return response;
	}

	public boolean hasTransactionData() {
		return (transactionData != null);
	}

	public boolean hasTransactionLoyaltySequenceID() {

		return !requestHeader.getLoyaltySequenceID().isEmpty();
	}

	public boolean hasFuelData() {

		return fuelPriceData != null;
	}

	public String getTransactionNumber() {
		return requestHeader.getLoyaltySequenceID();
	}

	public boolean hasPromotionBeenApplied() {

		boolean response = false;

		if (transactionData != null) {
			response = transactionData.hasPromotionBeenApplied();
		}

		return response;
	}


	public boolean isOutSideTransaction() {

		if (transactionData == null) {
			return true;
		} else if (transactionData.getTransactionHeader().outsideSalesFlag == 0) {
			return true;
		} else {
			return false;
		}

	}





	public static class FuelPriceData{

		protected String fuelGradeID;
		protected Amount12 regularSellPrice;
		protected BigInteger priceTierCode;
		protected String salesUOM;


		/**
		 * Default Constructor
		 */
		public FuelPriceData() {}


		/**
		 * Parameterized Constructor
		 * @param fuelGradeID
		 * @param regularSellPrice
		 * @param priceTierCode
		 * @param salesUOM
		 */

		public FuelPriceData(String fuelGradeID, Amount12 regularSellPrice, BigInteger priceTierCode, String salesUOM) {
			this.fuelGradeID = fuelGradeID;
			this.regularSellPrice = regularSellPrice;
			this.priceTierCode = priceTierCode;
			this.salesUOM = salesUOM;
		}




		/**
		 * Getters and Setters
		 * @return
		 */
		public String getFuelGradeID() {
			return fuelGradeID;
		}
		public void setFuelGradeID(String fuelGradeID) {
			this.fuelGradeID = fuelGradeID;
		}
		public Amount12 getRegularSellPrice() {
			return regularSellPrice;
		}
		public void setRegularSellPrice(Amount12 regularSellPrice) {
			this.regularSellPrice = regularSellPrice;
		}
		public BigInteger getPriceTierCode() {
			return priceTierCode;
		}
		public void setPriceTierCode(BigInteger priceTierCode) {
			this.priceTierCode = priceTierCode;
		}
		public String getSalesUOM() {
			return salesUOM;
		}
		public void setSalesUOM(String salesUOM) {
			this.salesUOM = salesUOM;
		}




	}

}
