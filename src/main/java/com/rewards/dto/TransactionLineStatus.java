package com.rewards.dto;

public enum TransactionLineStatus {

	NORMAL,
	CANCEL,	    
	RETURN,	    
	OTHER,	   
	VOID;

}
