package com.rewards.dto;

public class PriceOverride {
	
	protected Amount12 priceOverridePrice;
    protected String priceOverrideReason;

    /**
     * Gets the value of the priceOverridePrice property.
     * 
     * @return
     *     possible object is
     *     {@link Amount12 }
     *     
     */
    public Amount12 getPriceOverridePrice() {
        return priceOverridePrice;
    }

    /**
     * Sets the value of the priceOverridePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount12 }
     *     
     */
    public void setPriceOverridePrice(Amount12 value) {
        this.priceOverridePrice = value;
    }

    /**
     * Gets the value of the priceOverrideReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceOverrideReason() {
        return priceOverrideReason;
    }

    /**
     * Sets the value of the priceOverrideReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceOverrideReason(String value) {
        this.priceOverrideReason = value;
    }


}
