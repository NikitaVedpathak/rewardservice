package com.rewards.dto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.rewards.dto.RewardStatusCodes;
import com.rewards.dto.RewardTypeCodes;

/**
 * Represents a row in the DT_REWARD_POOL database table with each column mapped
 * to a property of this class.
 * @author Nikita
 *
 */


public class RewardPool {

	private Long dtRpId;
	private BigDecimal dtRpValue;
	private BigDecimal dtRpGals;
	private Date dtRpActivationDate;
	private Date dtRpExpirationDate;
	private int dtRpNumOfUses;
	private int dtRpPriority;
	private String dtRpRlRedemptionNum;
	private int dtRpMaxUsesPerDay;
	private int dtRpMaxUsesPerWeek;
	private int dtRpMaxUsesPerMonth;
	private BigDecimal dtRrTotalMaxGals;
	private BigDecimal dtRpMaxGalsPerDay;
	private BigDecimal dtRpMaxGalsPerWeek;
	private BigDecimal dtRpMaxGalsPerMonth;
	private String dtRpSettlementCode;
	private String dtRpDaysOfWeek;
	private OrdersDetail dtRpOrdersDetailId;
	private Programs dtRpProgId;
	private int dtRpStatusCode;
	private int dtRpTypeCode;
	private TokenPool dtRpTpId;
	private int dtRpNetworkId;


	/**
	 * Default constructor
	 */
	
	public RewardPool() {
		
		this.dtRpActivationDate = new Date();
		this.dtRpExpirationDate = new Date();
	}
	
	
	public RewardPool(String dtRpRlRedemptionNum) {
		
		SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.dtRpId = 0000000001L;
		this.dtRpValue = new BigDecimal(0.03);
		this.dtRpGals = new BigDecimal(20.000);
		try {
			this.dtRpActivationDate = dateformatter.parse("2015-09-22 00:00:00");
			this.dtRpExpirationDate = dateformatter.parse("2019-01-31 23:59:59");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.dtRpNumOfUses = 7998;
		this.dtRpPriority = 99;
		this.dtRpRlRedemptionNum = dtRpRlRedemptionNum;
		this.dtRpMaxUsesPerDay = 0;
		this.dtRpMaxUsesPerWeek = 0;
		this.dtRpMaxUsesPerMonth = 0;
		this.dtRrTotalMaxGals = new BigDecimal(0.000);
		this.dtRpMaxGalsPerDay = new BigDecimal(0.000);
		this.dtRpMaxGalsPerWeek = new BigDecimal(0.000);
		this.dtRpMaxGalsPerMonth = new BigDecimal(0.000);
		this.dtRpSettlementCode = "U";
		this.dtRpDaysOfWeek = "N/A";
		this.dtRpOrdersDetailId = new OrdersDetail() ;
		this.dtRpProgId = new Programs();
		this.dtRpStatusCode = 111;
		this.dtRpTypeCode = 111;
		this.dtRpTpId = new TokenPool();
		this.dtRpNetworkId = 2;		
		
	}


	/**
	 * Parameterized constructor
	 * @param dtRpId
	 * @param dtRpValue
	 * @param dtRpGals
	 * @param dtRpActivationDate
	 * @param dtRpExpirationDate
	 * @param dtRpNumOfUses
	 * @param dtRpPriority
	 * @param dtRpRlRedemptionNum
	 * @param dtRpMaxUsesPerDay
	 * @param dtRpMaxUsesPerWeek
	 * @param dtRpMaxUsesPerMonth
	 * @param dtRrTotalMaxGals
	 * @param dtRpMaxGalsPerDay
	 * @param dtRpMaxGalsPerWeek
	 * @param dtRpMaxGalsPerMonth
	 * @param dtRpSettlementCode
	 * @param dtRpDaysOfWeek
	 * @param dtRpOrdersDetailId
	 * @param dtRpProgId
	 * @param dtRpStatusCode
	 * @param dtRpTypeCode
	 * @param dtRpTpId
	 * @param dtRpNetworkId
	 */
	public RewardPool(Long dtRpId, BigDecimal dtRpValue, BigDecimal dtRpGals, Date dtRpActivationDate,
			Date dtRpExpirationDate, int dtRpNumOfUses, int dtRpPriority, String dtRpRlRedemptionNum,
			int dtRpMaxUsesPerDay, int dtRpMaxUsesPerWeek, int dtRpMaxUsesPerMonth, BigDecimal dtRrTotalMaxGals,
			BigDecimal dtRpMaxGalsPerDay, BigDecimal dtRpMaxGalsPerWeek, BigDecimal dtRpMaxGalsPerMonth,
			String dtRpSettlementCode, String dtRpDaysOfWeek, OrdersDetail dtRpOrdersDetailId, Programs dtRpProgId,
			int dtRpStatusCode, int dtRpTypeCode, TokenPool dtRpTpId,
			int dtRpNetworkId) {
		this.dtRpId = dtRpId;
		this.dtRpValue = dtRpValue;
		this.dtRpGals = dtRpGals;
		this.dtRpActivationDate = dtRpActivationDate;
		this.dtRpExpirationDate = dtRpExpirationDate;
		this.dtRpNumOfUses = dtRpNumOfUses;
		this.dtRpPriority = dtRpPriority;
		this.dtRpRlRedemptionNum = dtRpRlRedemptionNum;
		this.dtRpMaxUsesPerDay = dtRpMaxUsesPerDay;
		this.dtRpMaxUsesPerWeek = dtRpMaxUsesPerWeek;
		this.dtRpMaxUsesPerMonth = dtRpMaxUsesPerMonth;
		this.dtRrTotalMaxGals = dtRrTotalMaxGals;
		this.dtRpMaxGalsPerDay = dtRpMaxGalsPerDay;
		this.dtRpMaxGalsPerWeek = dtRpMaxGalsPerWeek;
		this.dtRpMaxGalsPerMonth = dtRpMaxGalsPerMonth;
		this.dtRpSettlementCode = dtRpSettlementCode;
		this.dtRpDaysOfWeek = dtRpDaysOfWeek;
		this.dtRpOrdersDetailId = dtRpOrdersDetailId;
		this.dtRpProgId = dtRpProgId;
		this.dtRpStatusCode = dtRpStatusCode;
		this.dtRpTypeCode = dtRpTypeCode;
		this.dtRpTpId = dtRpTpId;
		this.dtRpNetworkId = dtRpNetworkId;
	}

	/**
	 * Getters and Setters
	 */

	public Long getDtRpId() {
		return dtRpId;
	}


	public void setDtRpId(Long dtRpId) {
		this.dtRpId = dtRpId;
	}


	public BigDecimal getDtRpValue() {
		return dtRpValue;
	}


	public void setDtRpValue(BigDecimal dtRpValue) {
		this.dtRpValue = dtRpValue;
	}


	/**
	 * Gets the number of gallons that end users are allowed to redeem.
	 * @return number of gallons
	 */
	/*
	 * public BigDecimal getDtRpGals() { switch (this.getTypeCode()) { case PC: case
	 * PO: case GC: case GO: case DO: return this.dtRpGals; case PG: return
	 * this.dtRrTotalMaxGals.min(this.dtRpGals); default: return this.dtRpGals; } }
	 */

	public void setDtRpGals(BigDecimal dtRpGals) {
		this.dtRpGals = dtRpGals;
	}


	public Date getDtRpActivationDate() {
		return dtRpActivationDate;
	}


	public void setDtRpActivationDate(Date dtRpActivationDate) {
		this.dtRpActivationDate = dtRpActivationDate;
	}


	public Date getDtRpExpirationDate() {
		return dtRpExpirationDate;
	}


	public void setDtRpExpirationDate(Date dtRpExpirationDate) {
		this.dtRpExpirationDate = dtRpExpirationDate;
	}


	public int getDtRpNumOfUses() {
		return dtRpNumOfUses;
	}

	/**
	 * Sets the maximum number of uses of the Reward.
	 * @param dtRpNumOfUses the number of uses to set
	 */
	public void setDtRpNumOfUses(int dtRpNumOfUses) {
		//PPD-278 KSR SEp-20-2017 added the logic to avoid stacking up negetive value in database for unlimited rewards
		if(dtRpNumOfUses<0){
			this.dtRpNumOfUses = -1;  
		}else{
			this.dtRpNumOfUses = dtRpNumOfUses;
		}
	}


	public int getDtRpPriority() {
		return dtRpPriority;
	}


	public void setDtRpPriority(int dtRpPriority) {
		this.dtRpPriority = dtRpPriority;
	}


	public String getDtRpRlRedemptionNum() {
		return dtRpRlRedemptionNum;
	}


	public void setDtRpRlRedemptionNum(String dtRpRlRedemptionNum) {
		this.dtRpRlRedemptionNum = dtRpRlRedemptionNum;
	}


	public int getDtRpMaxUsesPerDay() {
		return dtRpMaxUsesPerDay;
	}


	public void setDtRpMaxUsesPerDay(int dtRpMaxUsesPerDay) {
		this.dtRpMaxUsesPerDay = dtRpMaxUsesPerDay;
	}


	public int getDtRpMaxUsesPerWeek() {
		return dtRpMaxUsesPerWeek;
	}


	public void setDtRpMaxUsesPerWeek(int dtRpMaxUsesPerWeek) {
		this.dtRpMaxUsesPerWeek = dtRpMaxUsesPerWeek;
	}


	public int getDtRpMaxUsesPerMonth() {
		return dtRpMaxUsesPerMonth;
	}


	public void setDtRpMaxUsesPerMonth(int dtRpMaxUsesPerMonth) {
		this.dtRpMaxUsesPerMonth = dtRpMaxUsesPerMonth;
	}


	public BigDecimal getDtRrTotalMaxGals() {
		return dtRrTotalMaxGals;
	}


	public void setDtRrTotalMaxGals(BigDecimal dtRrTotalMaxGals) {
		this.dtRrTotalMaxGals = dtRrTotalMaxGals;
	}


	public BigDecimal getDtRpMaxGalsPerDay() {
		return dtRpMaxGalsPerDay;
	}


	public void setDtRpMaxGalsPerDay(BigDecimal dtRpMaxGalsPerDay) {
		this.dtRpMaxGalsPerDay = dtRpMaxGalsPerDay;
	}


	public BigDecimal getDtRpMaxGalsPerWeek() {
		return dtRpMaxGalsPerWeek;
	}


	public void setDtRpMaxGalsPerWeek(BigDecimal dtRpMaxGalsPerWeek) {
		this.dtRpMaxGalsPerWeek = dtRpMaxGalsPerWeek;
	}


	public BigDecimal getDtRpMaxGalsPerMonth() {
		return dtRpMaxGalsPerMonth;
	}


	public void setDtRpMaxGalsPerMonth(BigDecimal dtRpMaxGalsPerMonth) {
		this.dtRpMaxGalsPerMonth = dtRpMaxGalsPerMonth;
	}


	public String getDtRpSettlementCode() {
		return dtRpSettlementCode;
	}


	public void setDtRpSettlementCode(String dtRpSettlementCode) {
		this.dtRpSettlementCode = dtRpSettlementCode;
	}


	public String getDtRpDaysOfWeek() {
		return dtRpDaysOfWeek;
	}


	public void setDtRpDaysOfWeek(String dtRpDaysOfWeek) {
		this.dtRpDaysOfWeek = dtRpDaysOfWeek;
	}


	public OrdersDetail getDtRpOrdersDetailId() {
		return dtRpOrdersDetailId;
	}


	public void setDtRpOrdersDetailId(OrdersDetail dtRpOrdersDetailId) {
		this.dtRpOrdersDetailId = dtRpOrdersDetailId;
	}


	public Programs getDtRpProgId() {
		return dtRpProgId;
	}


	public void setDtRpProgId(Programs dtRpProgId) {
		this.dtRpProgId = dtRpProgId;
	}


	public int getDtRpStatusCode() {
		return dtRpStatusCode;
	}


	public void setDtRpStatusCode(int dtRpStatusCode) {
		this.dtRpStatusCode = dtRpStatusCode;
	}


	public int getDtRpTypeCode() {
		return dtRpTypeCode;
	}


	public void setDtRpTypeCode(int dtRpTypeCode) {
		this.dtRpTypeCode = dtRpTypeCode;
	}


	public TokenPool getDtRpTpId() {
		return dtRpTpId;
	}


	public void setDtRpTpId(TokenPool dtRpTpId) {
		this.dtRpTpId = dtRpTpId;
	}


	public int getDtRpNetworkId() {
		return dtRpNetworkId;
	}


	public void setDtRpNetworkId(int dtRpNetworkId) {
		this.dtRpNetworkId = dtRpNetworkId;
	}



	/**
	 * Hash Code , equals and toString method for the class
	 */

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtRpId != null ? dtRpId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof RewardPool)) {
			return false;
		}
		RewardPool other = (RewardPool) object;
		if ((this.dtRpId == null && other.dtRpId != null) || (this.dtRpId != null && !this.dtRpId.equals(other.dtRpId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rewards.model.entity.RewardPool[ dtRpId=" + dtRpId + " ]";
	}

	/**
	 * 
	 * @return boolean
	 */

	public int getTypeCode() {
		return dtRpTypeCode;
	}

	public boolean isActivatedInTermOfStatusActivationDateTimeExpirationDateTime() {
		/*
		 * return RewardStatusCodes.ACTIVATED == this.getDtRpStatusCode() &&
		 * this.isActivatedInTermOfActivationDateTimeExpirationDateTime();
		 */
		
		return true;
	}

	public boolean isActivatedInTermOfActivationDateTimeExpirationDateTime() {
		return !new Date().before(this.getDtRpActivationDate())
				&& (this.getDtRpExpirationDate() == null || !new Date().after(this.getDtRpExpirationDate()));
	}


	/*
	 * // PPD-73 2016-09-15 MZ -zero out the gals of PC/PO or totalMaxGals of PG
	 * when gals(or totalMaxGals) * value < $0.01 public RewardPool
	 * zeroOutGalsWhenDiscountTooLess() {
	 * if(this.isDroptankDiscountLessThanOneCent()) { this.zeroOutGals(); }
	 * 
	 * return this; }
	 */

	/*private boolean isDroptankDiscountLessThanOneCent() {
		BigDecimal droptankDiscount = BigDecimal.ZERO;

		switch (this.getTypeCode()) {
		case PC:
		case PO:
		case DO:
			return false;
		case GC:
		case GO:
		case PG:
			droptankDiscount = this.getDtRpGals().multiply(this.getDtRpValue());
			break;
		default:
			droptankDiscount = BigDecimal.ONE;
			break;
		}

		return droptankDiscount.compareTo(new BigDecimal("0.01")) == -1 ? Boolean.TRUE : Boolean.FALSE;
	}


	private void zeroOutGals() {
		switch (this.getTypeCode()) {
		case GC:
		case GO:
			this.setDtRpGals(BigDecimal.ZERO);
			break;
		case PG:
			this.setDtRrTotalMaxGals(BigDecimal.ZERO);
			break;
		default:
			break;
		}*/


	}
