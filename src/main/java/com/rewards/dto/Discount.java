package com.rewards.dto;

public class Discount {


	protected String discountID;
	protected Amount12 discountAmount;
	protected String discountReason;
	protected String status;

	/**
	 * Gets the value of the discountID property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getDiscountID() {
		return discountID;
	}

	/**
	 * Sets the value of the discountID property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setDiscountID(String value) {
		this.discountID = value;
	}

	/**
	 * Gets the value of the discountAmount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * Sets the value of the discountAmount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setDiscountAmount(Amount12 value) {
		this.discountAmount = value;
	}

	/**
	 * Gets the value of the discountReason property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getDiscountReason() {
		return discountReason;
	}

	/**
	 * Sets the value of the discountReason property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setDiscountReason(String value) {
		this.discountReason = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getStatus() {
		if (status == null) {
			return "normal";
		} else {
			return status;
		}
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setStatus(String value) {
		this.status = value;
	}

}
