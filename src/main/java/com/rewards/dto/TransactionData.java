package com.rewards.dto;

import java.util.ArrayList;
import java.util.List;

public class TransactionData {
	
	protected TransactionHeader transactionHeader;
	protected TransactionDetailGroup transactionDetailGroup;

	public boolean hasFuelLine() {
		boolean response = false;

		List<TransactionLine> transLines = getTransactions();

		for(TransactionLine line : transLines)
			if (line.getFuelLine() != null) {
				response = true;

			}


		return response;
	}

	public List<TransactionLine> getTransactions() {
		if (transactionDetailGroup == null) {
			transactionDetailGroup = new TransactionDetailGroup();
		}

		return transactionDetailGroup.getTransactionLines();
	}

	public boolean isPrepayTransaction() {

		boolean response = false;

		if (hasFuelLine()) {

			List<TransactionLine> transLines = getTransactions();

			for(TransactionLine line : transLines) {

				if (line.status.equals(TransactionLineStatus.NORMAL) && line.getFuelLine() != null && line.getFuelLine().isPrepay()) {
					response = true;

				}
			}
		}

		return response;
	}

	/**
	 * Gets the value of the transactionHeader property.
	 *
	 * @return possible object is {@link TransactionHeader }
	 *
	 */
	 public TransactionHeader getTransactionHeader() {
		 return transactionHeader;
	 }

	 /**
	  * Sets the value of the transactionHeader property.
	  *
	  * @param value allowed object is {@link TransactionHeader }
	  *
	  */
	 public void setTransactionHeader(TransactionHeader value) {
		 this.transactionHeader = value;
	 }

	 /**
	  * Gets the value of the transactionDetailGroup property.
	  *
	  * @return possible object is {@link TransactionDetailGroup }
	  *
	  */
	 public TransactionDetailGroup getTransactionDetailGroup() {
		 if (transactionDetailGroup == null) {
			 transactionDetailGroup = new TransactionDetailGroup();
		 }
		 return transactionDetailGroup;
	 }

	 /**
	  * Sets the value of the transactionDetailGroup property.
	  *
	  * @param value allowed object is {@link TransactionDetailGroup }
	  *
	  */
	 public void setTransactionDetailGroup(TransactionDetailGroup value) {
		 this.transactionDetailGroup = value;
	 }

	 public int getNumberOfTransactions() {

		 int response = 0;

		 if (transactionDetailGroup != null) {
			 response = transactionDetailGroup.getTransactionLines().size();
		 }

		 return response;
	 }

	 public List<TransactionLine> getTransactionLines() {

		 return transactionDetailGroup.getTransactionLines();
	 }

	 public List<FuelLine> getFuelLines(){

		 List<FuelLine> response = new ArrayList<FuelLine>();
		 FuelLine fuelLine;

		 List<TransactionLine> transactionLines = getTransactionLines();

		 for(TransactionLine line : transactionLines){

			 fuelLine= line.getFuelLine();

			 if(fuelLine != null){
				 response.add(fuelLine);

			 }
		 }
		 return response;

	 }

	 public int getFuelLineNumberForAddReward(){

		 int response = -1;

		 List<TransactionLine> lines = getTransactionLines();

		 for (TransactionLine line : lines){

			 FuelLine fuelLine = line.getFuelLine();

			 if (line.getStatus().equals(TransactionLineStatus.NORMAL) && fuelLine != null){
				 response = line.getLineNumber();
			 }
		 }

		 return response;

	 }

	 public FuelLine getFuelLineForAddReward(){

		 FuelLine response = null;

		 List<TransactionLine> lines = getTransactionLines();

		 for (TransactionLine line : lines){

			 FuelLine fuelLine = line.getFuelLine();

			 if (line.getStatus().equals(TransactionLineStatus.NORMAL) && fuelLine != null){
				 response = fuelLine;
			 }
		 }

		 return response;
	 }

	 public boolean hasPromotionBeenApplied(){
		 boolean response = false;

		 List<TransactionLine> lines = getTransactionLines();

		 for (TransactionLine line : lines){

			 FuelLine fuelLine = line.getFuelLine();

			 if (line.getStatus().equals(TransactionLineStatus.NORMAL) && fuelLine != null && !fuelLine.getPromotions().isEmpty()){
				 response = true;
			 }
		 }

		 return response;

	 }

}
