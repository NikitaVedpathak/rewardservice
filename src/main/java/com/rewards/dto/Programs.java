package com.rewards.dto;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

/**
 *  Represents a row in the DT_PROGRAMS database table with each column mapped to
 * a property of this class.
 * @author Nikita
 *
 */


public class Programs {

	private Integer dtProgId;
	private String dtProgDesc;
	private BigDecimal dtProgValue;
	private BigDecimal dtProgGals;
	private int dtProgExpirationPeriod;
	private int dtProgNumOfUses;
	private int dtProgMaxUsesPerDay;
	private int dtProgMaxUsesPerWeek;
	private int dtProgMaxUsesPerMonth;
	private BigDecimal dtProgMaxGalsPerDay;
	private BigDecimal dtProgTotalMaxGals;
	private BigDecimal dtProgMaxGalsPerWeek;
	private BigDecimal dtProgMaxGalsPerMonth;
	private String dtProgDaysOfWeek;
	private Integer dtProgNetworkId;
	private int dtProgTypeCode; //changed type from RewardTypeCodesEntity to int
	private String dtProgSettlementCode;

	/**
	 * Default Constructor
	 */
	public Programs() {
		
		this.dtProgId = 9;
		this.dtProgDesc ="$0.05 Dropoints Test Cards" ;
		this.dtProgValue = new BigDecimal(0.05);
		this.dtProgGals = new BigDecimal(20.000);
		this.dtProgExpirationPeriod = 6;
		this.dtProgNumOfUses = 1;
		this.dtProgMaxUsesPerDay = 0;
		this.dtProgMaxUsesPerWeek = 0;
		this.dtProgMaxUsesPerMonth = 0;
		this.dtProgMaxGalsPerDay = new BigDecimal(0.000);
		this.dtProgTotalMaxGals = new BigDecimal(0.000);
		this.dtProgMaxGalsPerWeek = new BigDecimal(0.000);
		this.dtProgMaxGalsPerMonth = new BigDecimal(0.000);
		this.dtProgDaysOfWeek = "N/A";
		this.dtProgNetworkId = 1;
		this.dtProgTypeCode = 111;//dummy value
		this.dtProgSettlementCode = "F";
		
		
	}

	/**
	 * Parameterized constructor
	 * @param dtProgId
	 * @param dtProgDesc
	 * @param dtProgValue
	 * @param dtProgGals
	 * @param dtProgExpirationPeriod
	 * @param dtProgNumOfUses
	 * @param dtProgMaxUsesPerDay
	 * @param dtProgMaxUsesPerWeek
	 * @param dtProgMaxUsesPerMonth
	 * @param dtProgMaxGalsPerDay
	 * @param dtProgTotalMaxGals
	 * @param dtProgMaxGalsPerWeek
	 * @param dtProgMaxGalsPerMonth
	 * @param dtProgDaysOfWeek
	 * @param dtProgNetworkId
	 * @param dtProgTypeCode
	 * @param dtProgSettlementCode
	 */

	public Programs(Integer dtProgId, String dtProgDesc, BigDecimal dtProgValue, BigDecimal dtProgGals,
			int dtProgExpirationPeriod, int dtProgNumOfUses, int dtProgMaxUsesPerDay, int dtProgMaxUsesPerWeek,
			int dtProgMaxUsesPerMonth, BigDecimal dtProgMaxGalsPerDay, BigDecimal dtProgTotalMaxGals,
			BigDecimal dtProgMaxGalsPerWeek, BigDecimal dtProgMaxGalsPerMonth, String dtProgDaysOfWeek,
			Integer dtProgNetworkId, int dtProgTypeCode, String dtProgSettlementCode) {
		
		this.dtProgId = dtProgId;
		this.dtProgDesc = dtProgDesc;
		this.dtProgValue = dtProgValue;
		this.dtProgGals = dtProgGals;
		this.dtProgExpirationPeriod = dtProgExpirationPeriod;
		this.dtProgNumOfUses = dtProgNumOfUses;
		this.dtProgMaxUsesPerDay = dtProgMaxUsesPerDay;
		this.dtProgMaxUsesPerWeek = dtProgMaxUsesPerWeek;
		this.dtProgMaxUsesPerMonth = dtProgMaxUsesPerMonth;
		this.dtProgMaxGalsPerDay = dtProgMaxGalsPerDay;
		this.dtProgTotalMaxGals = dtProgTotalMaxGals;
		this.dtProgMaxGalsPerWeek = dtProgMaxGalsPerWeek;
		this.dtProgMaxGalsPerMonth = dtProgMaxGalsPerMonth;
		this.dtProgDaysOfWeek = dtProgDaysOfWeek;
		this.dtProgNetworkId = dtProgNetworkId;
		this.dtProgTypeCode = dtProgTypeCode;
		this.dtProgSettlementCode = dtProgSettlementCode;
	}

	/**
	 * Getters and Setters
	 * @return
	 */

	public Integer getDtProgId() {
		return dtProgId;
	}

	public void setDtProgId(Integer dtProgId) {
		this.dtProgId = dtProgId;
	}

	public String getDtProgDesc() {
		return dtProgDesc;
	}

	public void setDtProgDesc(String dtProgDesc) {
		this.dtProgDesc = dtProgDesc;
	}

	public BigDecimal getDtProgValue() {
		return dtProgValue;
	}

	public void setDtProgValue(BigDecimal dtProgValue) {
		this.dtProgValue = dtProgValue;
	}

	public BigDecimal getDtProgGals() {
		return dtProgGals;
	}

	public void setDtProgGals(BigDecimal dtProgGals) {
		this.dtProgGals = dtProgGals;
	}

	public int getDtProgExpirationPeriod() {
		return dtProgExpirationPeriod;
	}

	public void setDtProgExpirationPeriod(int dtProgExpirationPeriod) {
		this.dtProgExpirationPeriod = dtProgExpirationPeriod;
	}

	public int getDtProgNumOfUses() {
		return dtProgNumOfUses;
	}

	public void setDtProgNumOfUses(int dtProgNumOfUses) {
		this.dtProgNumOfUses = dtProgNumOfUses;
	}

	public int getDtProgMaxUsesPerDay() {
		return dtProgMaxUsesPerDay;
	}

	public void setDtProgMaxUsesPerDay(int dtProgMaxUsesPerDay) {
		this.dtProgMaxUsesPerDay = dtProgMaxUsesPerDay;
	}

	public int getDtProgMaxUsesPerWeek() {
		return dtProgMaxUsesPerWeek;
	}

	public void setDtProgMaxUsesPerWeek(int dtProgMaxUsesPerWeek) {
		this.dtProgMaxUsesPerWeek = dtProgMaxUsesPerWeek;
	}

	public int getDtProgMaxUsesPerMonth() {
		return dtProgMaxUsesPerMonth;
	}

	public void setDtProgMaxUsesPerMonth(int dtProgMaxUsesPerMonth) {
		this.dtProgMaxUsesPerMonth = dtProgMaxUsesPerMonth;
	}

	public BigDecimal getDtProgMaxGalsPerDay() {
		return dtProgMaxGalsPerDay;
	}

	public void setDtProgMaxGalsPerDay(BigDecimal dtProgMaxGalsPerDay) {
		this.dtProgMaxGalsPerDay = dtProgMaxGalsPerDay;
	}

	public BigDecimal getDtProgTotalMaxGals() {
		return dtProgTotalMaxGals;
	}

	public void setDtProgTotalMaxGals(BigDecimal dtProgTotalMaxGals) {
		this.dtProgTotalMaxGals = dtProgTotalMaxGals;
	}

	public BigDecimal getDtProgMaxGalsPerWeek() {
		return dtProgMaxGalsPerWeek;
	}

	public void setDtProgMaxGalsPerWeek(BigDecimal dtProgMaxGalsPerWeek) {
		this.dtProgMaxGalsPerWeek = dtProgMaxGalsPerWeek;
	}

	public BigDecimal getDtProgMaxGalsPerMonth() {
		return dtProgMaxGalsPerMonth;
	}

	public void setDtProgMaxGalsPerMonth(BigDecimal dtProgMaxGalsPerMonth) {
		this.dtProgMaxGalsPerMonth = dtProgMaxGalsPerMonth;
	}

	public String getDtProgDaysOfWeek() {
		return dtProgDaysOfWeek;
	}

	public void setDtProgDaysOfWeek(String dtProgDaysOfWeek) {
		this.dtProgDaysOfWeek = dtProgDaysOfWeek;
	}

	public Integer getDtProgNetworkId() {
		return dtProgNetworkId;
	}

	public void setDtProgNetworkId(Integer dtProgNetworkId) {
		this.dtProgNetworkId = dtProgNetworkId;
	}

	public int getDtProgTypeCode() {
		return dtProgTypeCode;
	}

	public void setDtProgTypeCode(int dtProgTypeCode) {
		this.dtProgTypeCode = dtProgTypeCode;
	}

	public String getDtProgSettlementCode() {
		return dtProgSettlementCode;
	}

	public void setDtProgSettlementCode(String dtProgSettlementCode) {
		this.dtProgSettlementCode = dtProgSettlementCode;
	}


	/**
	 * Hash Code, equals and toString method for class
	 */
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtProgId != null ? dtProgId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Programs)) {
			return false;
		}
		Programs other = (Programs) object;
		if ((this.dtProgId == null && other.dtProgId != null) || (this.dtProgId != null && !this.dtProgId.equals(other.dtProgId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rewards.model.entity.Programs[ dtProgId=" + dtProgId + " ]";
	}

}
