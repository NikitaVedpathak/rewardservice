package com.rewards.dto;

import org.springframework.stereotype.Component;


public enum RedemptionStatus {

	INACTIVE, ACTIVE;
}
