package com.rewards.dto;

import org.springframework.stereotype.Component;


public enum RewardStatusCodes {

	ACTIVATED,
	REDEEMED,
	DEACTIVATED,
	EXPIRED,
	LOCKED,
	PENDING_ACTIVATION,
	ERROR;
}
