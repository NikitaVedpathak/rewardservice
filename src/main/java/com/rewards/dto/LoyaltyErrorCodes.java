package com.rewards.dto;

import org.springframework.stereotype.Component;


public enum LoyaltyErrorCodes {
	
//  no token found by the give token Id
  TOKEN_NOT_FOUND,
  TOKEN_NOT_FOUND_PH,
//  no reward found by the given token Id
  REWARD_NOT_FOUND_PC,
  REWARD_NOT_FOUND_PO,
  REWARD_NOT_FOUND_GC,
  REWARD_NOT_FOUND_GO,
  REWARD_NOT_FOUND_DO,
  REWARD_NOT_FOUND_PG,
  REWARD_NOT_FOUND_PH,
//  reawrd(s) found, but they are in the locked status
  REWARD_IN_USE_PC,
  REWARD_IN_USE_PO,
  REWARD_IN_USE_GC,
  REWARD_IN_USE_GO,
  REWARD_IN_USE_DO,
  REWARD_IN_USE_PG,
  REWARD_IN_USE_PH,
//  reward(s) found, but they are in other invalid status
  REWARD_NOT_AVAILABLE_PC,
  REWARD_NOT_AVAILABLE_PO,
  REWARD_NOT_AVAILABLE_GC,
  REWARD_NOT_AVAILABLE_GO,
  REWARD_NOT_AVAILABLE_DO,
  REWARD_NOT_AVAILABLE_PG,
  REWARD_NOT_AVAILABLE_PH,
  OTHER;

}
