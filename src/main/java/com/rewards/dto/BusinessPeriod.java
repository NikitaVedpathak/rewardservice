package com.rewards.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.w3c.dom.Element;

public class BusinessPeriod {
	
	
	    protected XMLGregorianCalendar businessDate;
	    protected PrimaryReportPeriod primaryReportPeriod;
	    protected SecondaryReportPeriod secondaryReportPeriod;
	    protected XMLGregorianCalendar beginDate;
	    protected XMLGregorianCalendar beginTime;
	    protected XMLGregorianCalendar endDate;
	    protected XMLGregorianCalendar endTime;
	    protected List<BusinessPeriod.Extension> extensions;

	    /**
	     * Gets the value of the businessDate property.
	     *
	     * @return possible object is {@link XMLGregorianCalendar }
	     *
	     */
	    public XMLGregorianCalendar getBusinessDate() {
	        return businessDate;
	    }

	    /**
	     * Sets the value of the businessDate property.
	     *
	     * @param value allowed object is {@link XMLGregorianCalendar }
	     *
	     */
	    public void setBusinessDate(XMLGregorianCalendar value) {
	        this.businessDate = value;
	    }

	    /**
	     * Gets the value of the primaryReportPeriod property.
	     *
	     * @return possible object is {@link PrimaryReportPeriod }
	     *
	     */
	    public PrimaryReportPeriod getPrimaryReportPeriod() {
	        return primaryReportPeriod;
	    }

	    /**
	     * Sets the value of the primaryReportPeriod property.
	     *
	     * @param value allowed object is {@link PrimaryReportPeriod }
	     *
	     */
	    public void setPrimaryReportPeriod(PrimaryReportPeriod value) {
	        this.primaryReportPeriod = value;
	    }

	    /**
	     * Gets the value of the secondaryReportPeriod property.
	     *
	     * @return possible object is {@link SecondaryReportPeriod }
	     *
	     */
	    public SecondaryReportPeriod getSecondaryReportPeriod() {
	        return secondaryReportPeriod;
	    }

	    /**
	     * Sets the value of the secondaryReportPeriod property.
	     *
	     * @param value allowed object is {@link SecondaryReportPeriod }
	     *
	     */
	    public void setSecondaryReportPeriod(SecondaryReportPeriod value) {
	        this.secondaryReportPeriod = value;
	    }

	    /**
	     * Gets the value of the beginDate property.
	     *
	     * @return possible object is {@link XMLGregorianCalendar }
	     *
	     */
	    public XMLGregorianCalendar getBeginDate() {
	        return beginDate;
	    }

	    /**
	     * Sets the value of the beginDate property.
	     *
	     * @param value allowed object is {@link XMLGregorianCalendar }
	     *
	     */
	    public void setBeginDate(XMLGregorianCalendar value) {
	        this.beginDate = value;
	    }

	    /**
	     * Gets the value of the beginTime property.
	     *
	     * @return possible object is {@link XMLGregorianCalendar }
	     *
	     */
	    public XMLGregorianCalendar getBeginTime() {
	        return beginTime;
	    }

	    /**
	     * Sets the value of the beginTime property.
	     *
	     * @param value allowed object is {@link XMLGregorianCalendar }
	     *
	     */
	    public void setBeginTime(XMLGregorianCalendar value) {
	        this.beginTime = value;
	    }

	    /**
	     * Gets the value of the endDate property.
	     *
	     * @return possible object is {@link XMLGregorianCalendar }
	     *
	     */
	    public XMLGregorianCalendar getEndDate() {
	        return endDate;
	    }

	    /**
	     * Sets the value of the endDate property.
	     *
	     * @param value allowed object is {@link XMLGregorianCalendar }
	     *
	     */
	    public void setEndDate(XMLGregorianCalendar value) {
	        this.endDate = value;
	    }

	    /**
	     * Gets the value of the endTime property.
	     *
	     * @return possible object is {@link XMLGregorianCalendar }
	     *
	     */
	    public XMLGregorianCalendar getEndTime() {
	        return endTime;
	    }

	    /**
	     * Sets the value of the endTime property.
	     *
	     * @param value allowed object is {@link XMLGregorianCalendar }
	     *
	     */
	    public void setEndTime(XMLGregorianCalendar value) {
	        this.endTime = value;
	    }

	    /**
	     * Gets the value of the extensions property.
	     *
	     * <p>
	     * This accessor method returns a reference to the live list, not a
	     * snapshot. Therefore any modification you make to the returned list will
	     * be present inside the JAXB object. This is why there is not a
	     * <CODE>set</CODE> method for the extensions property.
	     *
	     * <p>
	     * For example, to add a new item, do as follows:
	     * <pre>
	     *    getExtensions().add(newItem);
	     * </pre>
	     *
	     *
	     * <p>
	     * Objects of the following type(s) are allowed in the list
	     * {@link BusinessPeriod.Extension }
	     *
	     *
	     */
	    public List<BusinessPeriod.Extension> getExtensions() {
	        if (extensions == null) {
	            extensions = new ArrayList<BusinessPeriod.Extension>();
	        }
	        return this.extensions;
	    }

	    /**
	     * <p>Java class for anonymous complex type.
	     *
	     * <p>The following schema fragment specifies the expected content contained
	     * within this class.
	     *
	     * <pre>
	     * &lt;complexType>
	     *   &lt;complexContent>
	     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	     *       &lt;sequence>
	     *         &lt;any processContents='lax' namespace='http://www.pcats.org/schema/naxml/loyalty/v01' maxOccurs="unbounded" minOccurs="0"/>
	     *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
	     *       &lt;/sequence>
	     *     &lt;/restriction>
	     *   &lt;/complexContent>
	     * &lt;/complexType>
	     * </pre>
	     *
	     *
	     */
	   
	    public static class Extension{

	        
	        protected List<Element> anies;

	        /**
	         * Gets the value of the anies property.
	         *
	         * <p>
	         * This accessor method returns a reference to the live list, not a
	         * snapshot. Therefore any modification you make to the returned list
	         * will be present inside the JAXB object. This is why there is not a
	         * <CODE>set</CODE> method for the anies property.
	         *
	         * <p>
	         * For example, to add a new item, do as follows:
	         * <pre>
	         *    getAnies().add(newItem);
	         * </pre>
	         *
	         *
	         * <p>
	         * Objects of the following type(s) are allowed in the list
	         * {@link Element }
	         *
	         *
	         */
	        public List<Element> getAnies() {
	            if (anies == null) {
	                anies = new ArrayList<Element>();
	            }
	            return this.anies;
	        }
	    }

}
