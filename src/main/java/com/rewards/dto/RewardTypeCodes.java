package com.rewards.dto;

import org.springframework.stereotype.Component;


public enum RewardTypeCodes {
	
	PC("PROMOTIONAL_CARD"),
    PO("PROMOTIONAL_CODE"),
    GC("GIFT_CARD"),
    GO("GIFT_CODE"),
    DO("DAILY_CODE"),
    PG("PROMOTIONAL_CODE_BASED_ON_GALLONS"),
    ERROR("ERROR");
    
    private String desc;
    
    RewardTypeCodes(String desc) {
        this.desc = desc;
    }
    
    public String getDesc() {
        return this.desc;
    }
    
    public String getCode() {
        return this.name();
    }

}
