package com.rewards.dto;

import org.springframework.stereotype.Component;

/**
 * POJO for Reward Type Code - defines the reward code ID, name and description
 * @author Nikita
 *
 */


public class RewardTypeCodesEntity {

	private Integer dtRtcId;
	private String dtRtcRewardTypeCode;
	private String dtRtcDesc;


	/**
	 * Default constructor
	 */
	public RewardTypeCodesEntity() {
	}
	
	
	/**
	 * Parameterized constructor
	 * @param dtRtcId
	 * @param dtRtcRewardTypeCode
	 * @param dtRtcDesc
	 */
	
	public RewardTypeCodesEntity(Integer dtRtcId, String dtRtcRewardTypeCode, String dtRtcDesc) {
		this.dtRtcId = dtRtcId;
		this.dtRtcRewardTypeCode = dtRtcRewardTypeCode;
		this.dtRtcDesc = dtRtcDesc;
	}




	/**
	 * Getters and Setters
	 * @return
	 */

	public Integer getDtRtcId() {
		return dtRtcId;
	}




	public void setDtRtcId(Integer dtRtcId) {
		this.dtRtcId = dtRtcId;
	}




	public String getDtRtcRewardTypeCode() {
		return dtRtcRewardTypeCode;
	}




	public void setDtRtcRewardTypeCode(String dtRtcRewardTypeCode) {
		this.dtRtcRewardTypeCode = dtRtcRewardTypeCode;
	}




	public String getDtRtcDesc() {
		return dtRtcDesc;
	}




	public void setDtRtcDesc(String dtRtcDesc) {
		this.dtRtcDesc = dtRtcDesc;
	}


	/**
	 * Hashcode method
	 */

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtRtcId != null ? dtRtcId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof RewardTypeCodesEntity)) {
			return false;
		}
		RewardTypeCodesEntity other = (RewardTypeCodesEntity) object;
		if ((this.dtRtcId == null && other.dtRtcId != null) || (this.dtRtcId != null && !this.dtRtcId.equals(other.dtRtcId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.model.entities.RewardTypeCodes[ dtRtcId=" + dtRtcId + " ]";
	}
}
