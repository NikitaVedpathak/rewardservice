package com.rewards.dto;

public class SecondaryReportPeriod {

	protected String value;
	protected String interval;

	/**
	 * Reporting period associated with the data. It may be a business day. It
	 * is represented by a sequence of integers that normally reset at some
	 * pre-determined time interval such as monthly, yearly etc.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the value property.
	 *
	 * @param value allowed object is {@link String }
	 *
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the value of the interval property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getInterval() {
		return interval;
	}

	/**
	 * Sets the value of the interval property.
	 *
	 * @param value allowed object is {@link String }
	 *
	 */
	public void setInterval(String value) {
		this.interval = value;
	}


}
