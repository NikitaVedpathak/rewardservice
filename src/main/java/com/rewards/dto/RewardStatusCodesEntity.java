package com.rewards.dto;

import org.springframework.stereotype.Component;

/**
 *  Represents a row in the DT_REWARD_STATUS_CODES database table with each column
 * mapped to a property of this class.
 * @author Nikita
 *
 */


public class RewardStatusCodesEntity {

	private Integer dtRscId;
	private String dtRscDesc;

	/**
	 * Default constructor
	 */
	public RewardStatusCodesEntity() {}

	/**
	 * Parameterized constructor
	 * @param dtRscId
	 * @param dtRscDesc
	 */
	public RewardStatusCodesEntity(Integer dtRscId, String dtRscDesc) {
		this.dtRscId = dtRscId;
		this.dtRscDesc = dtRscDesc;
	}

	/**
	 * Getters and Setters
	 */

	public Integer getDtRscId() {
		return dtRscId;
	}

	public void setDtRscId(Integer dtRscId) {
		this.dtRscId = dtRscId;
	}

	public String getDtRscDesc() {
		return dtRscDesc;
	}

	public void setDtRscDesc(String dtRscDesc) {
		this.dtRscDesc = dtRscDesc;
	}

	/**
	 * Hash Code and equals method for the class
	 */
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtRscId != null ? dtRscId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof RewardStatusCodesEntity)) {
			return false;
		}
		RewardStatusCodesEntity other = (RewardStatusCodesEntity) object;
		if ((this.dtRscId == null && other.dtRscId != null) || (this.dtRscId != null && !this.dtRscId.equals(other.dtRscId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.model.entities.RewardStatusCodes[ dtRscId=" + dtRscId + " ]";
	}

}
