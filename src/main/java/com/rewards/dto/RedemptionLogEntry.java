package com.rewards.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents a row in the DT_REDEMPTION_LOG database table with each column
 * mapped to a property of this class.
 * @author Nikita
 *
 */
public class RedemptionLogEntry {

	private String dtRlRedemptionNum;
	private String dtRlPosSeq;
	private Date dtRlRedemptionDate;
	private String dtRlPosId;
	private String dtRlTokenId;
	private long dtRlRewardId;
	private Boolean dtRlOutsideFlag;
	private Integer dtRlRegisterId;
	private Integer dtRlFuelPosition;
	private Integer dtRlCashierId;
	private Boolean dtRlPrepayFlag;
	private String dtRlFuelGrade;
	private Integer dtRlPriceTier;
	private String dtRlGradeDesc;
	private BigDecimal dtRlDiscPpg;
	private BigDecimal dtRlStreetPpg;
	private BigDecimal dtRlMaxGalsApproved;
	private BigDecimal dtRlGals;
	private BigDecimal dtRlDpTotDisc;
	private BigDecimal dtRlOtherTotDisc;
	private BigDecimal dtRlManualTotDisc;
	private BigDecimal dtRlTotCombDisc;	  
	private BigDecimal dtRlFuelTotWoutDisc;	   
	private BigDecimal dtRlFuelTotWDisc;	   
	private BigDecimal dtRlNonFuelAmt;
	private BigDecimal dtRlTaxAmt;
	private BigDecimal dtRlSaleTotWoutDisc;
	private BigDecimal dtRlSaleTotWDisc;
	private String dtRlTenderCode;
	private String dtRlTenderSubCode;
	private BigDecimal dtRlTransFee;
	private Date dtRlServerDate;
	private String dtRlBatchRefCode;
	private SettlementCodesEntity dtRlSettlementCode;
	private DiscTypeCodesEntity dtRlDiscType;
	private ServiceLevelCodesEntity dtRlSlCode;
	private RedemptionStateEntity dtRlRedemptionState;




	/**
	 * Default Constructor
	 */
	public RedemptionLogEntry() {}
	
	public RedemptionLogEntry(String redemptionnum) {
		this();
		this.dtRlRedemptionNum = redemptionnum;
	}
	
	

	public RedemptionLogEntry(String POSID, String LoyaltySequenceID) {
		
		//this.dtRlRedemptionNum = "DT_20150812_165913298_-289986437";
		this.dtRlRedemptionNum = LoyaltySequenceID;
		this.dtRlPosSeq = "00020263";
		this.dtRlRedemptionDate = null;
		this.dtRlPosId = POSID;
		this.dtRlTokenId = "636497100000000071";
		this.dtRlRewardId = 0000000133L;
		this.dtRlOutsideFlag = true;
		this.dtRlRegisterId = 9999;
		this.dtRlFuelPosition = 9999;
		this.dtRlCashierId = 9999;
		this.dtRlPrepayFlag = true;
		this.dtRlFuelGrade = "UNKNOWN";
		this.dtRlPriceTier = null ;
		this.dtRlGradeDesc = "UNKNOWN";
		this.dtRlDiscPpg = new BigDecimal(3.299);
		this.dtRlStreetPpg = new BigDecimal(3.299);
		this.dtRlMaxGalsApproved = null;
		this.dtRlGals = new BigDecimal(4.689);
		this.dtRlDpTotDisc = new BigDecimal(0.000);
		this.dtRlOtherTotDisc = new BigDecimal(0.000);
		this.dtRlManualTotDisc = new BigDecimal(0.00);
		this.dtRlTotCombDisc = new BigDecimal(0.47);
		this.dtRlFuelTotWoutDisc = new BigDecimal(15.47);
		this.dtRlFuelTotWDisc = new BigDecimal(15.00);
		this.dtRlNonFuelAmt = new BigDecimal(0.00);
		this.dtRlTaxAmt = new BigDecimal(0.00);
		this.dtRlSaleTotWoutDisc = new BigDecimal(15.47);
		this.dtRlSaleTotWDisc = new BigDecimal(15.00);
		this.dtRlTenderCode = "CASH";
		this.dtRlTenderSubCode = "UNKNOWN";
		this.dtRlTransFee = new BigDecimal(0.1172250);
		this.dtRlServerDate = null;
		this.dtRlBatchRefCode = "DTBAT:";
		this.dtRlSettlementCode = null;
		this.dtRlDiscType = null;
		this.dtRlSlCode = null;
		this.dtRlRedemptionState = null;
		
	}



	/**
	 * Parameterized constructor
	 * @param dtRlRedemptionNum
	 * @param dtRlPosSeq
	 * @param dtRlRedemptionDate
	 * @param dtRlPosId
	 * @param dtRlTokenId
	 * @param dtRlRewardId
	 * @param dtRlOutsideFlag
	 * @param dtRlRegisterId
	 * @param dtRlFuelPosition
	 * @param dtRlCashierId
	 * @param dtRlPrepayFlag
	 * @param dtRlFuelGrade
	 * @param dtRlPriceTier
	 * @param dtRlGradeDesc
	 * @param dtRlDiscPpg
	 * @param dtRlStreetPpg
	 * @param dtRlMaxGalsApproved
	 * @param dtRlGals
	 * @param dtRlDpTotDisc
	 * @param dtRlOtherTotDisc
	 * @param dtRlManualTotDisc
	 * @param dtRlTotCombDisc
	 * @param dtRlFuelTotWoutDisc
	 * @param dtRlFuelTotWDisc
	 * @param dtRlNonFuelAmt
	 * @param dtRlTaxAmt
	 * @param dtRlSaleTotWoutDisc
	 * @param dtRlSaleTotWDisc
	 * @param dtRlTenderCode
	 * @param dtRlTenderSubCode
	 * @param dtRlTransFee
	 * @param dtRlServerDate
	 * @param dtRlBatchRefCode
	 * @param dtRlSettlementCode
	 * @param dtRlDiscType
	 * @param dtRlSlCode
	 * @param dtRlRedemptionState
	 */

	public RedemptionLogEntry(String dtRlRedemptionNum, String dtRlPosSeq, Date dtRlRedemptionDate, String dtRlPosId,
			String dtRlTokenId, long dtRlRewardId, Boolean dtRlOutsideFlag, Integer dtRlRegisterId,
			Integer dtRlFuelPosition, Integer dtRlCashierId, Boolean dtRlPrepayFlag, String dtRlFuelGrade,
			Integer dtRlPriceTier, String dtRlGradeDesc, BigDecimal dtRlDiscPpg, BigDecimal dtRlStreetPpg,
			BigDecimal dtRlMaxGalsApproved, BigDecimal dtRlGals, BigDecimal dtRlDpTotDisc, BigDecimal dtRlOtherTotDisc,
			BigDecimal dtRlManualTotDisc, BigDecimal dtRlTotCombDisc, BigDecimal dtRlFuelTotWoutDisc,
			BigDecimal dtRlFuelTotWDisc, BigDecimal dtRlNonFuelAmt, BigDecimal dtRlTaxAmt,
			BigDecimal dtRlSaleTotWoutDisc, BigDecimal dtRlSaleTotWDisc, String dtRlTenderCode,
			String dtRlTenderSubCode, BigDecimal dtRlTransFee, Date dtRlServerDate, String dtRlBatchRefCode,
			SettlementCodesEntity dtRlSettlementCode, DiscTypeCodesEntity dtRlDiscType,
			ServiceLevelCodesEntity dtRlSlCode, RedemptionStateEntity dtRlRedemptionState) {
		this.dtRlRedemptionNum = dtRlRedemptionNum;
		this.dtRlPosSeq = dtRlPosSeq;
		this.dtRlRedemptionDate = dtRlRedemptionDate;
		this.dtRlPosId = dtRlPosId;
		this.dtRlTokenId = dtRlTokenId;
		this.dtRlRewardId = dtRlRewardId;
		this.dtRlOutsideFlag = dtRlOutsideFlag;
		this.dtRlRegisterId = dtRlRegisterId;
		this.dtRlFuelPosition = dtRlFuelPosition;
		this.dtRlCashierId = dtRlCashierId;
		this.dtRlPrepayFlag = dtRlPrepayFlag;
		this.dtRlFuelGrade = dtRlFuelGrade;
		this.dtRlPriceTier = dtRlPriceTier;
		this.dtRlGradeDesc = dtRlGradeDesc;
		this.dtRlDiscPpg = dtRlDiscPpg;
		this.dtRlStreetPpg = dtRlStreetPpg;
		this.dtRlMaxGalsApproved = dtRlMaxGalsApproved;
		this.dtRlGals = dtRlGals;
		this.dtRlDpTotDisc = dtRlDpTotDisc;
		this.dtRlOtherTotDisc = dtRlOtherTotDisc;
		this.dtRlManualTotDisc = dtRlManualTotDisc;
		this.dtRlTotCombDisc = dtRlTotCombDisc;
		this.dtRlFuelTotWoutDisc = dtRlFuelTotWoutDisc;
		this.dtRlFuelTotWDisc = dtRlFuelTotWDisc;
		this.dtRlNonFuelAmt = dtRlNonFuelAmt;
		this.dtRlTaxAmt = dtRlTaxAmt;
		this.dtRlSaleTotWoutDisc = dtRlSaleTotWoutDisc;
		this.dtRlSaleTotWDisc = dtRlSaleTotWDisc;
		this.dtRlTenderCode = dtRlTenderCode;
		this.dtRlTenderSubCode = dtRlTenderSubCode;
		this.dtRlTransFee = dtRlTransFee;
		this.dtRlServerDate = dtRlServerDate;
		this.dtRlBatchRefCode = dtRlBatchRefCode;
		this.dtRlSettlementCode = dtRlSettlementCode;
		this.dtRlDiscType = dtRlDiscType;
		this.dtRlSlCode = dtRlSlCode;
		this.dtRlRedemptionState = dtRlRedemptionState;
	}


	/**
	 * Getters and Setters
	 * @return
	 */

	public String getDtRlRedemptionNum() {
		return dtRlRedemptionNum;
	}



	public void setDtRlRedemptionNum(String dtRlRedemptionNum) {
		this.dtRlRedemptionNum = dtRlRedemptionNum;
	}



	public String getDtRlPosSeq() {
		return dtRlPosSeq;
	}



	public void setDtRlPosSeq(String dtRlPosSeq) {
		this.dtRlPosSeq = dtRlPosSeq;
	}



	public Date getDtRlRedemptionDate() {
		return dtRlRedemptionDate;
	}



	public void setDtRlRedemptionDate(Date dtRlRedemptionDate) {
		this.dtRlRedemptionDate = dtRlRedemptionDate;
	}



	public String getDtRlPosId() {
		return dtRlPosId;
	}



	public void setDtRlPosId(String dtRlPosId) {
		this.dtRlPosId = dtRlPosId;
	}



	public String getDtRlTokenId() {
		return dtRlTokenId;
	}



	public void setDtRlTokenId(String dtRlTokenId) {
		this.dtRlTokenId = dtRlTokenId;
	}



	public long getDtRlRewardId() {
		return dtRlRewardId;
	}



	public void setDtRlRewardId(long dtRlRewardId) {
		this.dtRlRewardId = dtRlRewardId;
	}



	public Boolean getDtRlOutsideFlag() {
		return dtRlOutsideFlag;
	}



	public void setDtRlOutsideFlag(Boolean dtRlOutsideFlag) {
		this.dtRlOutsideFlag = dtRlOutsideFlag;
	}



	public Integer getDtRlRegisterId() {
		return dtRlRegisterId;
	}



	public void setDtRlRegisterId(Integer dtRlRegisterId) {
		this.dtRlRegisterId = dtRlRegisterId;
	}



	public Integer getDtRlFuelPosition() {
		return dtRlFuelPosition;
	}



	public void setDtRlFuelPosition(Integer dtRlFuelPosition) {
		this.dtRlFuelPosition = dtRlFuelPosition;
	}



	public Integer getDtRlCashierId() {
		return dtRlCashierId;
	}



	public void setDtRlCashierId(Integer dtRlCashierId) {
		this.dtRlCashierId = dtRlCashierId;
	}



	public Boolean getDtRlPrepayFlag() {
		return dtRlPrepayFlag;
	}



	public void setDtRlPrepayFlag(Boolean dtRlPrepayFlag) {
		this.dtRlPrepayFlag = dtRlPrepayFlag;
	}



	public String getDtRlFuelGrade() {
		return dtRlFuelGrade;
	}



	public void setDtRlFuelGrade(String dtRlFuelGrade) {
		this.dtRlFuelGrade = dtRlFuelGrade;
	}



	public Integer getDtRlPriceTier() {
		return dtRlPriceTier;
	}



	public void setDtRlPriceTier(Integer dtRlPriceTier) {
		this.dtRlPriceTier = dtRlPriceTier;
	}



	public String getDtRlGradeDesc() {
		return dtRlGradeDesc;
	}



	public void setDtRlGradeDesc(String dtRlGradeDesc) {
		this.dtRlGradeDesc = dtRlGradeDesc;
	}



	public BigDecimal getDtRlDiscPpg() {
		return dtRlDiscPpg;
	}



	public void setDtRlDiscPpg(BigDecimal dtRlDiscPpg) {
		this.dtRlDiscPpg = dtRlDiscPpg;
	}



	public BigDecimal getDtRlStreetPpg() {
		return dtRlStreetPpg;
	}



	public void setDtRlStreetPpg(BigDecimal dtRlStreetPpg) {
		this.dtRlStreetPpg = dtRlStreetPpg;
	}



	public BigDecimal getDtRlMaxGalsApproved() {
		return dtRlMaxGalsApproved;
	}



	public void setDtRlMaxGalsApproved(BigDecimal dtRlMaxGalsApproved) {
		this.dtRlMaxGalsApproved = dtRlMaxGalsApproved;
	}



	public BigDecimal getDtRlGals() {
		return dtRlGals;
	}



	public void setDtRlGals(BigDecimal dtRlGals) {
		this.dtRlGals = dtRlGals;
	}



	public BigDecimal getDtRlDpTotDisc() {
		return dtRlDpTotDisc;
	}



	public void setDtRlDpTotDisc(BigDecimal dtRlDpTotDisc) {
		this.dtRlDpTotDisc = dtRlDpTotDisc;
	}



	public BigDecimal getDtRlOtherTotDisc() {
		return dtRlOtherTotDisc;
	}



	public void setDtRlOtherTotDisc(BigDecimal dtRlOtherTotDisc) {
		this.dtRlOtherTotDisc = dtRlOtherTotDisc;
	}



	public BigDecimal getDtRlManualTotDisc() {
		return dtRlManualTotDisc;
	}



	public void setDtRlManualTotDisc(BigDecimal dtRlManualTotDisc) {
		this.dtRlManualTotDisc = dtRlManualTotDisc;
	}



	public BigDecimal getDtRlTotCombDisc() {
		return dtRlTotCombDisc;
	}



	public void setDtRlTotCombDisc(BigDecimal dtRlTotCombDisc) {
		this.dtRlTotCombDisc = dtRlTotCombDisc;
	}



	public BigDecimal getDtRlFuelTotWoutDisc() {
		return dtRlFuelTotWoutDisc;
	}



	public void setDtRlFuelTotWoutDisc(BigDecimal dtRlFuelTotWoutDisc) {
		this.dtRlFuelTotWoutDisc = dtRlFuelTotWoutDisc;
	}



	public BigDecimal getDtRlFuelTotWDisc() {
		return dtRlFuelTotWDisc;
	}



	public void setDtRlFuelTotWDisc(BigDecimal dtRlFuelTotWDisc) {
		this.dtRlFuelTotWDisc = dtRlFuelTotWDisc;
	}



	public BigDecimal getDtRlNonFuelAmt() {
		return dtRlNonFuelAmt;
	}



	public void setDtRlNonFuelAmt(BigDecimal dtRlNonFuelAmt) {
		this.dtRlNonFuelAmt = dtRlNonFuelAmt;
	}



	public BigDecimal getDtRlTaxAmt() {
		return dtRlTaxAmt;
	}



	public void setDtRlTaxAmt(BigDecimal dtRlTaxAmt) {
		this.dtRlTaxAmt = dtRlTaxAmt;
	}



	public BigDecimal getDtRlSaleTotWoutDisc() {
		return dtRlSaleTotWoutDisc;
	}



	public void setDtRlSaleTotWoutDisc(BigDecimal dtRlSaleTotWoutDisc) {
		this.dtRlSaleTotWoutDisc = dtRlSaleTotWoutDisc;
	}



	public BigDecimal getDtRlSaleTotWDisc() {
		return dtRlSaleTotWDisc;
	}



	public void setDtRlSaleTotWDisc(BigDecimal dtRlSaleTotWDisc) {
		this.dtRlSaleTotWDisc = dtRlSaleTotWDisc;
	}



	public String getDtRlTenderCode() {
		return dtRlTenderCode;
	}



	public void setDtRlTenderCode(String dtRlTenderCode) {
		this.dtRlTenderCode = dtRlTenderCode;
	}



	public String getDtRlTenderSubCode() {
		return dtRlTenderSubCode;
	}



	public void setDtRlTenderSubCode(String dtRlTenderSubCode) {
		this.dtRlTenderSubCode = dtRlTenderSubCode;
	}



	public BigDecimal getDtRlTransFee() {
		return dtRlTransFee;
	}



	public void setDtRlTransFee(BigDecimal dtRlTransFee) {
		this.dtRlTransFee = dtRlTransFee;
	}



	public Date getDtRlServerDate() {
		return dtRlServerDate;
	}



	public void setDtRlServerDate(Date dtRlServerDate) {
		this.dtRlServerDate = dtRlServerDate;
	}



	public String getDtRlBatchRefCode() {
		return dtRlBatchRefCode;
	}



	public void setDtRlBatchRefCode(String dtRlBatchRefCode) {
		this.dtRlBatchRefCode = dtRlBatchRefCode;
	}



	public SettlementCodesEntity getDtRlSettlementCode() {
		return dtRlSettlementCode;
	}



	public void setDtRlSettlementCode(SettlementCodesEntity dtRlSettlementCode) {
		this.dtRlSettlementCode = dtRlSettlementCode;
	}



	public DiscTypeCodesEntity getDtRlDiscType() {
		return dtRlDiscType;
	}



	public void setDtRlDiscType(DiscTypeCodesEntity dtRlDiscType) {
		this.dtRlDiscType = dtRlDiscType;
	}



	public ServiceLevelCodesEntity getDtRlSlCode() {
		return dtRlSlCode;
	}



	public void setDtRlSlCode(ServiceLevelCodesEntity dtRlSlCode) {
		this.dtRlSlCode = dtRlSlCode;
	}



	public RedemptionStateEntity getDtRlRedemptionState() {
		return dtRlRedemptionState;
	}



	public void setDtRlRedemptionState(RedemptionStateEntity dtRlRedemptionState) {
		this.dtRlRedemptionState = dtRlRedemptionState;
	}

	/**
	 * Hash Code , equals and toString method
	 */

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtRlRedemptionNum != null ? dtRlRedemptionNum.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof RedemptionLogEntry)) {
			return false;
		}
		RedemptionLogEntry other = (RedemptionLogEntry) object;
		if ((this.dtRlRedemptionNum == null && other.dtRlRedemptionNum != null) || (this.dtRlRedemptionNum != null && !this.dtRlRedemptionNum.equals(other.dtRlRedemptionNum))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rlms.model.entities.RedemptionLogEntry[ dtRlRedemptionNum=" + dtRlRedemptionNum + " ]";
	}

	//	    PPD-85 2016-08-25 MZ -added this method to remove unwanted special characters from the given string
	private String removeSpecialCharacters(String inputStr) {
		if (inputStr == null) {
			return "";
		}
		return inputStr.replace(',', ' ');
	}

	//This methods sets every field to zero

	public RedemptionLogEntry zeroOutSaleData() {
		this.setDtRlGals(BigDecimal.ZERO);
		this.setDtRlDpTotDisc(BigDecimal.ZERO);
		this.setDtRlOtherTotDisc(BigDecimal.ZERO);
		this.setDtRlManualTotDisc(BigDecimal.ZERO);
		this.setDtRlTotCombDisc(BigDecimal.ZERO);
		this.setDtRlFuelTotWDisc(BigDecimal.ZERO);
		this.setDtRlFuelTotWoutDisc(BigDecimal.ZERO);
		this.setDtRlTaxAmt(BigDecimal.ZERO);
		this.setDtRlSaleTotWDisc(BigDecimal.ZERO);
		this.setDtRlSaleTotWoutDisc(BigDecimal.ZERO);
		this.setDtRlNonFuelAmt(BigDecimal.ZERO);
		this.setDtRlTotCombDisc(BigDecimal.ZERO);
		this.setDtRlTransFee(BigDecimal.ZERO);
		return this;
	}



}
