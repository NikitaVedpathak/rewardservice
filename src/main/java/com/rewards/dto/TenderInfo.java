package com.rewards.dto;

public class TenderInfo {
	
	
	    protected String tenderCode;
	    protected String tenderSubCode;
	    protected String isoPrefix;
	    protected String loyaltyRewardID;
	    protected Amount12 tenderAmount;
	    protected int changeFlag;

	    /**
	     * Gets the value of the tenderCode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getTenderCode() {
	        return tenderCode;
	    }

	    /**
	     * Sets the value of the tenderCode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setTenderCode(String value) {
	        this.tenderCode = value;
	    }

	    /**
	     * Gets the value of the tenderSubCode property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getTenderSubCode() {
	        return tenderSubCode;
	    }

	    /**
	     * Sets the value of the tenderSubCode property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setTenderSubCode(String value) {
	        this.tenderSubCode = value;
	    }

	    /**
	     * Gets the value of the isoPrefix property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getISOPrefix() {
	        return isoPrefix;
	    }

	    /**
	     * Sets the value of the isoPrefix property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setISOPrefix(String value) {
	        this.isoPrefix = value;
	    }

	    /**
	     * Gets the value of the loyaltyRewardID property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link String }
	     *     
	     */
	    public String getLoyaltyRewardID() {
	        return loyaltyRewardID;
	    }

	    /**
	     * Sets the value of the loyaltyRewardID property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link String }
	     *     
	     */
	    public void setLoyaltyRewardID(String value) {
	        this.loyaltyRewardID = value;
	    }

	    /**
	     * Gets the value of the tenderAmount property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link Amount12 }
	     *     
	     */
	    public Amount12 getTenderAmount() {
	        return tenderAmount;
	    }

	    /**
	     * Sets the value of the tenderAmount property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link Amount12 }
	     *     
	     */
	    public void setTenderAmount(Amount12 value) {
	        this.tenderAmount = value;
	    }

	    /**
	     * Gets the value of the changeFlag property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link FlagDefNo }
	     *     
	     */
	    public int getChangeFlag() {
	        return changeFlag;
	    }

	    /**
	     * Sets the value of the changeFlag property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link FlagDefNo }
	     *     
	     */
	    public void setChangeFlag(int value) {
	        this.changeFlag = value;
	    }

}
