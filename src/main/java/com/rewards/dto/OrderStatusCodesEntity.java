package com.rewards.dto;

import org.springframework.stereotype.Component;

/**
 * Represents a row in the DT_ORDER_STATUS_CODES database table with each column mapped to a
 * property of this class.
 * @author Nikita
 *
 */


public class OrderStatusCodesEntity {

	private Integer dtOscId;
	private String dtOscDesc;

	/**
	 * Default constructor
	 */
	public OrderStatusCodesEntity() {}

	/**
	 * Parameterized constructor
	 * @param dtOscId
	 * @param dtOscDesc
	 */
	public OrderStatusCodesEntity(Integer dtOscId, String dtOscDesc) {
		super();
		this.dtOscId = dtOscId;
		this.dtOscDesc = dtOscDesc;
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */

	public Integer getDtOscId() {
		return dtOscId;
	}

	public void setDtOscId(Integer dtOscId) {
		this.dtOscId = dtOscId;
	}

	public String getDtOscDesc() {
		return dtOscDesc;
	}

	public void setDtOscDesc(String dtOscDesc) {
		this.dtOscDesc = dtOscDesc;
	}

	/**
	 * Hash code and equals method for the class
	 */
	@Override
    public int hashCode() {
        int hash = 0;
        hash += (dtOscId != null ? dtOscId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the OrderStatusCodesEntity fields are not set
        if (!(object instanceof OrderStatusCodesEntity)) {
            return false;
        }
        OrderStatusCodesEntity other = (OrderStatusCodesEntity) object;
        if ((this.dtOscId == null && other.dtOscId != null) || (this.dtOscId != null && !this.dtOscId.equals(other.dtOscId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.OrderStatusCodesEntity[ dtOscId=" + dtOscId + " ]";
    }

	

}
