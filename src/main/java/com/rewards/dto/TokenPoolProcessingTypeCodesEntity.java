package com.rewards.dto;

import org.springframework.stereotype.Component;

/**
 * Represents a row in the DT_TOKEN_POOL_PROCESSING_TYPE_CODES database table with each column mapped to
 * a property of this class. 
 * @author Nikita
 *
 */


public class TokenPoolProcessingTypeCodesEntity {

	private Integer dtTpptcId;
	private String dtTpptcDesc;

	/**
	 * Default constructor
	 */
	public TokenPoolProcessingTypeCodesEntity() {}

	/**
	 * Parameterized constructor
	 * @param dtTpptcId
	 * @param dtTpptcDesc
	 */
	public TokenPoolProcessingTypeCodesEntity(Integer dtTpptcId, String dtTpptcDesc) {
		super();
		this.dtTpptcId = dtTpptcId;
		this.dtTpptcDesc = dtTpptcDesc;
	}

	/**
	 * Getters and Setters
	 */

	public Integer getDtTpptcId() {
		return dtTpptcId;
	}

	public void setDtTpptcId(Integer dtTpptcId) {
		this.dtTpptcId = dtTpptcId;
	}

	public String getDtTpptcDesc() {
		return dtTpptcDesc;
	}

	public void setDtTpptcDesc(String dtTpptcDesc) {
		this.dtTpptcDesc = dtTpptcDesc;
	}
	
	/**
	 * Hash Code and equals method for the class
	 */

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtTpptcId != null ? dtTpptcId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof TokenPoolProcessingTypeCodesEntity)) {
			return false;
		}
		TokenPoolProcessingTypeCodesEntity other = (TokenPoolProcessingTypeCodesEntity) object;
		if ((this.dtTpptcId == null && other.dtTpptcId != null) || (this.dtTpptcId != null && !this.dtTpptcId.equals(other.dtTpptcId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rewards.mode.entities.TokenPoolProcessingTypeCodesEntity[ dtTpptcId=" + dtTpptcId + " ]";
	}


}
