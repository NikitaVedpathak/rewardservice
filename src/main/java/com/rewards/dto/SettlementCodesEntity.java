package com.rewards.dto;

import java.util.List;

/**
 *  Represents a row in the `DT_SETTLEMENT_CODES` database table with each column mapped to a
 * property of this class
 *
 */
public class SettlementCodesEntity {

	private Integer dtSettlementCode;
	private String dtSettlementCodeDescription;
	private List<String> redemptionLogList;

	/**
	 * Default Constructor
	 */
	public SettlementCodesEntity() {}


	/**
	 * Parameterized constructor
	 * @param dtSettlementCode
	 * @param dtSettlementCodeDescription
	 * @param redemptionLogList
	 */

	public SettlementCodesEntity(Integer dtSettlementCode, String dtSettlementCodeDescription,
			List<String> redemptionLogList) {
		super();
		this.dtSettlementCode = dtSettlementCode;
		this.dtSettlementCodeDescription = dtSettlementCodeDescription;
		this.redemptionLogList = redemptionLogList;
	}



	/**
	 * Getters and Setters
	 * @return
	 */

	public Integer getDtSettlementCode() {
		return dtSettlementCode;
	}

	public void setDtSettlementCode(Integer dtSettlementCode) {
		this.dtSettlementCode = dtSettlementCode;
	}

	public String getDtSettlementCodeDescription() {
		return dtSettlementCodeDescription;
	}

	public void setDtSettlementCodeDescription(String dtSettlementCodeDescription) {
		this.dtSettlementCodeDescription = dtSettlementCodeDescription;
	}

	public List<String> getRedemptionLogList() {
		return redemptionLogList;
	}

	public void setRedemptionLogList(List<String> redemptionLogList) {
		this.redemptionLogList = redemptionLogList;
	}


	/**
	 * Hash Code , equals and toString methods
	 */
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtSettlementCode != null ? dtSettlementCode.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof SettlementCodesEntity)) {
			return false;
		}
		SettlementCodesEntity other = (SettlementCodesEntity) object;
		if ((this.dtSettlementCode == null && other.dtSettlementCode != null) || (this.dtSettlementCode != null && !this.dtSettlementCode.equals(other.dtSettlementCode))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rlms.model.enums.SettlementCodes[ dtSettlementCode=" + dtSettlementCode + " ]";
	}

}
