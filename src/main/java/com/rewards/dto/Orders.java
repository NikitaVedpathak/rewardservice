package com.rewards.dto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Represents a row in the DT_ORDERS database table with each column mapped to a
 * property of this class.
 * @author Nikita
 *
 */


public class Orders {

	private Integer dtOrderSysNum;
	private String dtOrderClientNum;
	private int dtOrderClientId;
	private String dtOrderDesc;
	private BigDecimal dtOrderTotalPrice;
	private BigDecimal dtOrderShippingCost;
	private String dtOrderShippingAddr1;
	private String dtOrderShippingAddr2;
	private String dtOrderShippingCity;
	private String dtOrderShippingState;
	private String dtOrderShippingZipCode;
	private String dtOrderBillingAddr1;
	private String dtOrderBillingAddr2;
	private String dtOrderBillingCity;
	private String dtOrderBillingState;
	private String dtOrderBillingZipCode;
	private Date dtOrderTimePlaced;
	private Date dtOrderLastUpdate;
	private String dtOrderEditor;
	private int dtOrderStatusCode; //changed type from OrderStatusCodesEntity to int
	private List<String> ordersDetailList; // changed to String from OrdersDetails generics for List


	/**
	 * Default Constructor
	 */

	public Orders() {
		
		this.dtOrderSysNum = 0000000001;
		this.dtOrderClientNum = "TEST001";
		this.dtOrderClientId = 0000000003;
		this.dtOrderDesc = "N/A";
		this.dtOrderTotalPrice = new BigDecimal(0.00);
		this.dtOrderShippingCost = new BigDecimal(0.00);
		this.dtOrderShippingAddr1 = "N/A";
		this.dtOrderShippingAddr2 = "N/A";
		this.dtOrderShippingCity = "N/A";
		this.dtOrderShippingState = "IL";
		this.dtOrderShippingZipCode = "N/A";
		this.dtOrderBillingAddr1 = "N/A";
		this.dtOrderBillingAddr2 = "N/A";
		this.dtOrderBillingCity = "N/A";
		this.dtOrderBillingState = "N/A";
		this.dtOrderBillingZipCode = "N/A";
		
		SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			this.dtOrderTimePlaced = dateformatter.parse("2015-08-11 14:56:49");
			this.dtOrderLastUpdate = dateformatter.parse("2016-04-18 15:00:38");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.dtOrderEditor = "N/A";
		this.dtOrderStatusCode = 111;
		
		List<String> list = new ArrayList<String>();
		list.add("dummy data");
		this.ordersDetailList = list;
		
		
	}

	/**
	 * Parameterized Constructor
	 * @param dtOrderSysNum
	 * @param dtOrderClientNum
	 * @param dtOrderClientId
	 * @param dtOrderDesc
	 * @param dtOrderTotalPrice
	 * @param dtOrderShippingCost
	 * @param dtOrderShippingAddr1
	 * @param dtOrderShippingAddr2
	 * @param dtOrderShippingCity
	 * @param dtOrderShippingState
	 * @param dtOrderShippingZipCode
	 * @param dtOrderBillingAddr1
	 * @param dtOrderBillingAddr2
	 * @param dtOrderBillingCity
	 * @param dtOrderBillingState
	 * @param dtOrderBillingZipCode
	 * @param dtOrderTimePlaced
	 * @param dtOrderLastUpdate
	 * @param dtOrderEditor
	 * @param dtOrderStatusCode
	 * @param ordersDetailList
	 */

	public Orders(Integer dtOrderSysNum, String dtOrderClientNum, int dtOrderClientId, String dtOrderDesc,
			BigDecimal dtOrderTotalPrice, BigDecimal dtOrderShippingCost, String dtOrderShippingAddr1,
			String dtOrderShippingAddr2, String dtOrderShippingCity, String dtOrderShippingState,
			String dtOrderShippingZipCode, String dtOrderBillingAddr1, String dtOrderBillingAddr2,
			String dtOrderBillingCity, String dtOrderBillingState, String dtOrderBillingZipCode, Date dtOrderTimePlaced,
			Date dtOrderLastUpdate, String dtOrderEditor, int dtOrderStatusCode,
			List<OrdersDetail> ordersDetailList) {
		this.dtOrderSysNum = dtOrderSysNum;
		this.dtOrderClientNum = dtOrderClientNum;
		this.dtOrderClientId = dtOrderClientId;
		this.dtOrderDesc = dtOrderDesc;
		this.dtOrderTotalPrice = dtOrderTotalPrice;
		this.dtOrderShippingCost = dtOrderShippingCost;
		this.dtOrderShippingAddr1 = dtOrderShippingAddr1;
		this.dtOrderShippingAddr2 = dtOrderShippingAddr2;
		this.dtOrderShippingCity = dtOrderShippingCity;
		this.dtOrderShippingState = dtOrderShippingState;
		this.dtOrderShippingZipCode = dtOrderShippingZipCode;
		this.dtOrderBillingAddr1 = dtOrderBillingAddr1;
		this.dtOrderBillingAddr2 = dtOrderBillingAddr2;
		this.dtOrderBillingCity = dtOrderBillingCity;
		this.dtOrderBillingState = dtOrderBillingState;
		this.dtOrderBillingZipCode = dtOrderBillingZipCode;
		this.dtOrderTimePlaced = dtOrderTimePlaced;
		this.dtOrderLastUpdate = dtOrderLastUpdate;
		this.dtOrderEditor = dtOrderEditor;
		this.dtOrderStatusCode = dtOrderStatusCode;
	//	this.ordersDetailList = ordersDetailList;
	}

	/**
	 * Getters and Setters
	 */

	public Integer getDtOrderSysNum() {
		return dtOrderSysNum;
	}

	public void setDtOrderSysNum(Integer dtOrderSysNum) {
		this.dtOrderSysNum = dtOrderSysNum;
	}

	public String getDtOrderClientNum() {
		return dtOrderClientNum;
	}

	public void setDtOrderClientNum(String dtOrderClientNum) {
		this.dtOrderClientNum = dtOrderClientNum;
	}

	public int getDtOrderClientId() {
		return dtOrderClientId;
	}

	public void setDtOrderClientId(int dtOrderClientId) {
		this.dtOrderClientId = dtOrderClientId;
	}

	public String getDtOrderDesc() {
		return dtOrderDesc;
	}

	public void setDtOrderDesc(String dtOrderDesc) {
		this.dtOrderDesc = dtOrderDesc;
	}

	public BigDecimal getDtOrderTotalPrice() {
		return dtOrderTotalPrice;
	}

	public void setDtOrderTotalPrice(BigDecimal dtOrderTotalPrice) {
		this.dtOrderTotalPrice = dtOrderTotalPrice;
	}

	public BigDecimal getDtOrderShippingCost() {
		return dtOrderShippingCost;
	}

	public void setDtOrderShippingCost(BigDecimal dtOrderShippingCost) {
		this.dtOrderShippingCost = dtOrderShippingCost;
	}

	public String getDtOrderShippingAddr1() {
		return dtOrderShippingAddr1;
	}

	public void setDtOrderShippingAddr1(String dtOrderShippingAddr1) {
		this.dtOrderShippingAddr1 = dtOrderShippingAddr1;
	}

	public String getDtOrderShippingAddr2() {
		return dtOrderShippingAddr2;
	}

	public void setDtOrderShippingAddr2(String dtOrderShippingAddr2) {
		this.dtOrderShippingAddr2 = dtOrderShippingAddr2;
	}

	public String getDtOrderShippingCity() {
		return dtOrderShippingCity;
	}

	public void setDtOrderShippingCity(String dtOrderShippingCity) {
		this.dtOrderShippingCity = dtOrderShippingCity;
	}

	public String getDtOrderShippingState() {
		return dtOrderShippingState;
	}

	public void setDtOrderShippingState(String dtOrderShippingState) {
		this.dtOrderShippingState = dtOrderShippingState;
	}

	public String getDtOrderShippingZipCode() {
		return dtOrderShippingZipCode;
	}

	public void setDtOrderShippingZipCode(String dtOrderShippingZipCode) {
		this.dtOrderShippingZipCode = dtOrderShippingZipCode;
	}

	public String getDtOrderBillingAddr1() {
		return dtOrderBillingAddr1;
	}

	public void setDtOrderBillingAddr1(String dtOrderBillingAddr1) {
		this.dtOrderBillingAddr1 = dtOrderBillingAddr1;
	}

	public String getDtOrderBillingAddr2() {
		return dtOrderBillingAddr2;
	}

	public void setDtOrderBillingAddr2(String dtOrderBillingAddr2) {
		this.dtOrderBillingAddr2 = dtOrderBillingAddr2;
	}

	public String getDtOrderBillingCity() {
		return dtOrderBillingCity;
	}

	public void setDtOrderBillingCity(String dtOrderBillingCity) {
		this.dtOrderBillingCity = dtOrderBillingCity;
	}

	public String getDtOrderBillingState() {
		return dtOrderBillingState;
	}

	public void setDtOrderBillingState(String dtOrderBillingState) {
		this.dtOrderBillingState = dtOrderBillingState;
	}

	public String getDtOrderBillingZipCode() {
		return dtOrderBillingZipCode;
	}

	public void setDtOrderBillingZipCode(String dtOrderBillingZipCode) {
		this.dtOrderBillingZipCode = dtOrderBillingZipCode;
	}

	public Date getDtOrderTimePlaced() {
		return dtOrderTimePlaced;
	}

	public void setDtOrderTimePlaced(Date dtOrderTimePlaced) {
		this.dtOrderTimePlaced = dtOrderTimePlaced;
	}

	public Date getDtOrderLastUpdate() {
		return dtOrderLastUpdate;
	}

	public void setDtOrderLastUpdate(Date dtOrderLastUpdate) {
		this.dtOrderLastUpdate = dtOrderLastUpdate;
	}

	public String getDtOrderEditor() {
		return dtOrderEditor;
	}

	public void setDtOrderEditor(String dtOrderEditor) {
		this.dtOrderEditor = dtOrderEditor;
	}

	public int getDtOrderStatusCode() {
		return dtOrderStatusCode;
	}

	public void setDtOrderStatusCode(int dtOrderStatusCode) {
		this.dtOrderStatusCode = dtOrderStatusCode;
	}

	/*
	 * public List<OrdersDetail> getOrdersDetailList() { return ordersDetailList; }
	 * 
	 * public void setOrdersDetailList(List<OrdersDetail> ordersDetailList) {
	 * this.ordersDetailList = ordersDetailList; }
	 */

	/**
	 * Hash code and equals method for the class
	 */

	public int hashCode() {
		int hash = 0;
		hash += (dtOrderSysNum != null ? dtOrderSysNum.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the OrderStatusCodesEntity fields are not set
		if (!(object instanceof Orders)) {
			return false;
		}
		Orders other = (Orders) object;
		if ((this.dtOrderSysNum == null && other.dtOrderSysNum != null) || (this.dtOrderSysNum != null && !this.dtOrderSysNum.equals(other.dtOrderSysNum))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "com.droptank.rewards.model.entity.Orders[ dtOrderSysNum=" + dtOrderSysNum + " ]";
	}


}
