package com.rewards.dto;

public class TransactionTax {
	
	
    protected String taxLevelID;
    protected Amount12 taxableSalesAmount;
    protected Amount12 taxCollectedAmount;
    protected Amount12 taxableSalesRefundedAmount;
    protected Amount12 taxRefundedAmount;

    /**
     * Gets the value of the taxLevelID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxLevelID() {
        return taxLevelID;
    }

    /**
     * Sets the value of the taxLevelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxLevelID(String value) {
        this.taxLevelID = value;
    }

    /**
     * Gets the value of the taxableSalesAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount12 }
     *     
     */
    public Amount12 getTaxableSalesAmount() {
        return taxableSalesAmount;
    }

    /**
     * Sets the value of the taxableSalesAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount12 }
     *     
     */
    public void setTaxableSalesAmount(Amount12 value) {
        this.taxableSalesAmount = value;
    }

    /**
     * Gets the value of the taxCollectedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount12 }
     *     
     */
    public Amount12 getTaxCollectedAmount() {
        return taxCollectedAmount;
    }

    /**
     * Sets the value of the taxCollectedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount12 }
     *     
     */
    public void setTaxCollectedAmount(Amount12 value) {
        this.taxCollectedAmount = value;
    }

    /**
     * Gets the value of the taxableSalesRefundedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount12 }
     *     
     */
    public Amount12 getTaxableSalesRefundedAmount() {
        return taxableSalesRefundedAmount;
    }

    /**
     * Sets the value of the taxableSalesRefundedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount12 }
     *     
     */
    public void setTaxableSalesRefundedAmount(Amount12 value) {
        this.taxableSalesRefundedAmount = value;
    }

    /**
     * Gets the value of the taxRefundedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount12 }
     *     
     */
    public Amount12 getTaxRefundedAmount() {
        return taxRefundedAmount;
    }

    /**
     * Sets the value of the taxRefundedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount12 }
     *     
     */
    public void setTaxRefundedAmount(Amount12 value) {
        this.taxRefundedAmount = value;
    }


}
