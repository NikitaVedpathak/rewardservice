package com.rewards.dto;

import java.util.ArrayList;
import java.util.List;

public class ItemLine {

	protected ItemCode itemCode;
	protected String merchandiseCode;
	protected String description;
	protected Amount12 actualSalesPrice;
	protected Amount12 regularSellPrice;
	protected SellUnit sellingUnits;
	protected Quantity12 salesQuantity;
	protected Amount12 salesAmount;
	protected String salesUOM;
	protected PriceOverride priceOverride;
	protected List<Promotion> promotions;
	protected List<Discount> discounts;
	protected List<ItemTax> itemTaxes;
	protected Integer linkedFromLineNumber;
	protected String paymentSystemsProductCode;
	protected int discountable;

	/**
	 * Gets the value of the itemCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link ItemCode }
	 *     
	 */
	public ItemCode getItemCode() {
		return itemCode;
	}

	/**
	 * Sets the value of the itemCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link ItemCode }
	 *     
	 */
	public void setItemCode(ItemCode value) {
		this.itemCode = value;
	}

	/**
	 * Gets the value of the merchandiseCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMerchandiseCode() {
		return merchandiseCode;
	}

	/**
	 * Sets the value of the merchandiseCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMerchandiseCode(String value) {
		this.merchandiseCode = value;
	}

	/**
	 * Gets the value of the description property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setDescription(String value) {
		this.description = value;
	}

	/**
	 * Gets the value of the actualSalesPrice property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getActualSalesPrice() {
		return actualSalesPrice;
	}

	/**
	 * Sets the value of the actualSalesPrice property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setActualSalesPrice(Amount12 value) {
		this.actualSalesPrice = value;
	}

	/**
	 * Gets the value of the regularSellPrice property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getRegularSellPrice() {
		return regularSellPrice;
	}

	/**
	 * Sets the value of the regularSellPrice property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setRegularSellPrice(Amount12 value) {
		this.regularSellPrice = value;
	}

	/**
	 * Gets the value of the sellingUnits property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link SellUnit }
	 *     
	 */
	public SellUnit getSellingUnits() {
		return sellingUnits;
	}

	/**
	 * Sets the value of the sellingUnits property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link SellUnit }
	 *     
	 */
	public void setSellingUnits(SellUnit value) {
		this.sellingUnits = value;
	}

	/**
	 * Gets the value of the salesQuantity property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Quantity12 }
	 *     
	 */
	public Quantity12 getSalesQuantity() {
		return salesQuantity;
	}

	/**
	 * Sets the value of the salesQuantity property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Quantity12 }
	 *     
	 */
	public void setSalesQuantity(Quantity12 value) {
		this.salesQuantity = value;
	}

	/**
	 * Gets the value of the salesAmount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Amount12 }
	 *     
	 */
	public Amount12 getSalesAmount() {
		return salesAmount;
	}

	/**
	 * Sets the value of the salesAmount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Amount12 }
	 *     
	 */
	public void setSalesAmount(Amount12 value) {
		this.salesAmount = value;
	}

	/**
	 * Gets the value of the salesUOM property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSalesUOM() {
		return salesUOM;
	}

	/**
	 * Sets the value of the salesUOM property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSalesUOM(String value) {
		this.salesUOM = value;
	}

	/**
	 * Gets the value of the priceOverride property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link PriceOverride }
	 *     
	 */
	public PriceOverride getPriceOverride() {
		return priceOverride;
	}

	/**
	 * Sets the value of the priceOverride property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link PriceOverride }
	 *     
	 */
	public void setPriceOverride(PriceOverride value) {
		this.priceOverride = value;
	}

	/**
	 * Gets the value of the promotions property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the promotions property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getPromotions().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Promotion }
	 * 
	 * 
	 */
	public List<Promotion> getPromotions() {
		if (promotions == null) {
			promotions = new ArrayList<Promotion>();
		}
		return this.promotions;
	}

	/**
	 * Gets the value of the discounts property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the discounts property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getDiscounts().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Discount }
	 * 
	 * 
	 */
	public List<Discount> getDiscounts() {
		if (discounts == null) {
			discounts = new ArrayList<Discount>();
		}
		return this.discounts;
	}

	/**
	 * Gets the value of the itemTaxes property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the itemTaxes property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getItemTaxes().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ItemTax }
	 * 
	 * 
	 */
	public List<ItemTax> getItemTaxes() {
		if (itemTaxes == null) {
			itemTaxes = new ArrayList<ItemTax>();
		}
		return this.itemTaxes;
	}

	/**
	 * Gets the value of the linkedFromLineNumber property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Integer }
	 *     
	 */
	public Integer getLinkedFromLineNumber() {
		return linkedFromLineNumber;
	}

	/**
	 * Sets the value of the linkedFromLineNumber property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Integer }
	 *     
	 */
	public void setLinkedFromLineNumber(Integer value) {
		this.linkedFromLineNumber = value;
	}

	/**
	 * Gets the value of the paymentSystemsProductCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPaymentSystemsProductCode() {
		return paymentSystemsProductCode;
	}

	/**
	 * Sets the value of the paymentSystemsProductCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPaymentSystemsProductCode(String value) {
		this.paymentSystemsProductCode = value;
	}

	/**
	 * Gets the value of the discountable property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link YesNo }
	 *     
	 */
	public int getDiscountable() {
		return discountable;
	}

	/**
	 * Sets the value of the discountable property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link YesNo }
	 *     
	 */
	public void setDiscountable(int value) {
		this.discountable = value;
	}

}
