package com.rewards.dto;

import java.util.List;

/**
 *  Represents a row in the DT_SERVICE_LEVEL_CODES database table with each column mapped to a
 * property of this class.
 *
 *
 */
public class ServiceLevelCodesEntity {


	private Integer dtServiceLevelCode;
	private String dtServiceLevelCodeDescription;
	private List<String> redemptionLogList;

	/**
	 * Default constructor
	 */
	public ServiceLevelCodesEntity() {}

	/**
	 * Parameterized constructor
	 * @param dtServiceLevelCode
	 * @param dtServiceLevelCodeDescription
	 * @param redemptionLogList
	 */
	public ServiceLevelCodesEntity(Integer dtServiceLevelCode, String dtServiceLevelCodeDescription,
			List<String> redemptionLogList) {
		this.dtServiceLevelCode = dtServiceLevelCode;
		this.dtServiceLevelCodeDescription = dtServiceLevelCodeDescription;
		this.redemptionLogList = redemptionLogList;
	}


	/**
	 * Getters and Setters
	 */
	public Integer getDtServiceLevelCode() {
		return dtServiceLevelCode;
	}

	public void setDtServiceLevelCode(Integer dtServiceLevelCode) {
		this.dtServiceLevelCode = dtServiceLevelCode;
	}

	public String getDtServiceLevelCodeDescription() {
		return dtServiceLevelCodeDescription;
	}

	public void setDtServiceLevelCodeDescription(String dtServiceLevelCodeDescription) {
		this.dtServiceLevelCodeDescription = dtServiceLevelCodeDescription;
	}

	public List<String> getRedemptionLogList() {
		return redemptionLogList;
	}

	public void setRedemptionLogList(List<String> redemptionLogList) {
		this.redemptionLogList = redemptionLogList;
	}

	/**
	 * Hash Code, equals and toString method
	 */
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtServiceLevelCode != null ? dtServiceLevelCode.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ServiceLevelCodesEntity)) {
			return false;
		}
		ServiceLevelCodesEntity other = (ServiceLevelCodesEntity) object;
		if ((this.dtServiceLevelCode == null && other.dtServiceLevelCode != null) || (this.dtServiceLevelCode != null && !this.dtServiceLevelCode.equals(other.dtServiceLevelCode))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rlms.model.entities.ServiceLevelCodes[ dtServiceLevelCode=" + dtServiceLevelCode + " ]";
	}




}
