package com.rewards.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rewards.dto.RewardTypeCodes;

/**
 * Represents a row in the DT_TOKEN_POOL database table with each column mapped
 * to a property of this class.
 * @author Nikita
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenPool {

 
	private String dtTpId;
	private int dtTpTypeCode; //changed from RewardTypeCodesEntity to int
	private int dtTpNetworkId;
	private String dtTpActivationCode;
	private List<String> rewardPoolList; //changed the generics to String, previous - RewardPool
	private int dtTpTpptcProcessingTypeCode;//changed from TokenPoolProcessingTypeCodesEntity to int
	

	/**
	 * Default Constructor
	 */
	public TokenPool() {
		
		
		this.dtTpId = "0011230512";
		this.dtTpNetworkId = 2;
		this.dtTpActivationCode = "0011230512";
		List<String> list = new ArrayList<String>();
		list.add("dummydata");
		this.setRewardPoolList(list);
		this.dtTpTpptcProcessingTypeCode = 111;
		this.dtTpTypeCode = 111;
		
	}

	/**
	 * Parameterized constructor
	 * @param dtTpId
	 * @param dtTpNetworkId
	 * @param dtTpActivationCode
	 * @param rewardPoolList
	 * @param dtTpTpptcProcessingTypeCode
	 * @param dtTpTypeCode
	 */
	public TokenPool(String dtTpId, int dtTpNetworkId, String dtTpActivationCode, List<RewardPool> rewardPoolList,
			int dtTpTpptcProcessingTypeCode, int dtTpTypeCode) {
		this.dtTpId = dtTpId;
		this.dtTpNetworkId = dtTpNetworkId;
		this.dtTpActivationCode = dtTpActivationCode;
	//	this.rewardPoolList = rewardPoolList;
		this.dtTpTpptcProcessingTypeCode = dtTpTpptcProcessingTypeCode;
		this.dtTpTypeCode = dtTpTypeCode;
	}

	/**
	 * Getters and Setters 
	 */

	public String getDtTpId() {
		return dtTpId;
	}

	public void setDtTpId(String dtTpId) {
		this.dtTpId = dtTpId;
	}

	public int getDtTpNetworkId() {
		return dtTpNetworkId;
	}

	public void setDtTpNetworkId(int dtTpNetworkId) {
		this.dtTpNetworkId = dtTpNetworkId;
	}

	public String getDtTpActivationCode() {
		return dtTpActivationCode;
	}

	public void setDtTpActivationCode(String dtTpActivationCode) {
		this.dtTpActivationCode = dtTpActivationCode;
	}
	
	public List<String> getRewardPoolList() {
		return rewardPoolList;
	}

	public void setRewardPoolList(List<String> rewardPoolList) {
		this.rewardPoolList = rewardPoolList;
	}

	/*
	 * public List<RewardPool> getRewardPoolList() { return rewardPoolList; }
	 * 
	 * public void setRewardPoolList(List<RewardPool> rewardPoolList) {
	 * this.rewardPoolList = rewardPoolList; }
	 */

	public int getDtTpTpptcProcessingTypeCode() {
		return dtTpTpptcProcessingTypeCode;
	}

	public void setDtTpTpptcProcessingTypeCode(int dtTpTpptcProcessingTypeCode) {
		this.dtTpTpptcProcessingTypeCode = dtTpTpptcProcessingTypeCode;
	}

	public int getDtTpTypeCode() {
		return dtTpTypeCode;
	}

	public void setDtTpTypeCode(int dtTpTypeCode) {
		this.dtTpTypeCode = dtTpTypeCode;
	}

	/**
	 * Hash Code ,equals and toString method for the class
	 */
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dtTpId != null ? dtTpId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof TokenPool)) {
			return false;
		}
		TokenPool other = (TokenPool) object;
		if ((this.dtTpId == null && other.dtTpId != null) || (this.dtTpId != null && !this.dtTpId.equals(other.dtTpId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.droptank.rewards.model.entity.TokenPool[ dtTpId=" + dtTpId + " ]";
	}

	


	/*
	 * public TokenPool deprioritizeDailyCodeReward() { List<RewardPool> rewardList
	 * = this.getRewardPoolList(); for (RewardPool reward : rewardList) { if (1 ==
	 * reward.getDtRpPriority() && RewardTypeCodes.DO.equals(reward.getTypeCode()))
	 * { reward.setDtRpPriority(99); break; } } return this; }
	 */

}
